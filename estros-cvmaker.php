<?php
/**
 * Plugin Name: Estros CV Maker
 * Plugin URI: http://estros.gr
 * Description: This plugin adds a cv form through a shortcode that enables guests to create their CV in a professional format.
 * Version: 1.0
 * Author: Estros.gr
 * Author URI: http://estros.gr
 * License: GPL2
 */

// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}


function cvmakerform($atts, $content){
  $buttontext = "Preview CV!";
  if(!empty($content))
    $buttontext = $content;
  extract( shortcode_atts( array(
  ), $atts, 'estros-cvmaker' ) );
  ?>
  
  <?php 
	
	include 'frontend-form.php'; 
}

function cvmaker_wptuts_scripts_important()
{
  wp_enqueue_script('jquery');
  wp_register_style( 'cvestrosStyle', plugins_url( '/css/estros-cvmaker.css', __FILE__ ), array(), '20120208', 'all' );
  wp_enqueue_style( 'cvestrosStyle' );
}

add_action('admin_menu', 'estroscvmaker_admin');
add_action( 'admin_enqueue_scripts', 'cvmaker_wptuts_scripts_important' );
 
function estroscvmaker_admin()
{
  add_menu_page( 'Estros CV Maker', 'CV Maker', 'administrator', 'estros-cvmaker', 'estroscvmaker_init', 'dashicons-money' );
}
 
function estroscvmaker_init(){
  ?>
  <div class="wrap">
    <div style="padding:5px; display: inline-block; width:42%;">   
        <h2>CV Maker</h2>
      <div class="welcome-panel">
        <a href="https://estros.gr" target="_blank">
          <img src="http://estros.gr/logo.png" height="45">
        </a>
        <div style="width:100%; font-size: 18px; font-weight: bold;">Plugin Description</div>
        <p>This plugin adds a CV Wizard through a shortcode that enables subscribers to create their CV in several predefined formats.</p>
      </div>
     
      <div class="welcome-panel">
        <h3>Settings</h3>
        <?php include 'cvmaker-admin.php'; ?>
      </div>
  	</div>
    <div class="welcome-panel" style="vertical-align: top; width: 53%; display: inline-block;">
      <h2>How to use the shortcode</h2>
      <p>Place this shortcode in any post/page/product description to get the CV Maker Wizard!.</p>
      <p>
        <code>
        	[estros-cvmaker][/estros-cvmaker]
        </code>
      </p>
  	</div>
  </div>
  <?php
}
add_action( 'admin_init', 'cvmaker_plugin_settings' );
function cvmaker_plugin_settings() {
  
}
add_action( 'wp_enqueue_scripts', 'cvmaker_wptuts_scripts_important', 5 );
add_shortcode('estros-cvmaker', 'cvmakerform');