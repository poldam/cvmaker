<link rel="stylesheet" type="text/css" href="<?php echo plugin_dir_url( __FILE__ );  ?>css/font-awesome.min.css" />
<?php

function getFileList($dir, $username, $userid)
{
	// array to hold return value
	$retval = array();
	// add trailing slash if missing
	if(substr($dir, -1) != "/") $dir .= "/";
	// open directory for reading
	$d = new DirectoryIterator($dir) or die("getFileList: Failed opening directory $dir for reading");
	foreach($d as $fileinfo) {
		// skip hidden files
		if($fileinfo->isDot()) continue;
		if (strpos($fileinfo, $userid.'-'.$username) !== false)
		{
			$shortname = explode("]", $fileinfo);
			$shortname = $shortname[0];
			$retval[] = array(
				'name' => "{$fileinfo}",
				"shortname" => $shortname."]",
				'type' => ($fileinfo->getType() == "dir") ? "dir" : mime_content_type($fileinfo->getRealPath()),
				'size' => $fileinfo->getSize(),
				'lastmod' => $fileinfo->getMTime()
			);
		}
	}
	return $retval;
}

if (is_user_logged_in()) 
{
	global $current_user;
	get_currentuserinfo();
}
?>

<script type="application/javascript">

function clearcvfields(allfields)
{
	if(allfields)
	{
		jQuery('#cvsavename').val('');
		jQuery('#fullname').val('');
		jQuery('#emailaddresses').val('');
		jQuery('#phonenumbers').val('');
		jQuery('#websites').val('');
		jQuery('#adline1').val('');
		jQuery('#adline2').val('');
		jQuery('#adline3').val('');
		jQuery('#nationality').val('');
		jQuery('#contactdescription').val('');
		jQuery('#hobbies').val('');
		jQuery('#jobtitle').val('');
		jQuery('#birthdate').val('');

		jQuery("#insertEducationList").html("");
		jQuery("#insertWorkExList").html("");
		jQuery("#insertPublicationsList").html("");
		
		jQuery("#insertQualificationsList").html("");
		jQuery("#insertAwardsList").html("");
		jQuery("#insertCertificationsList").html("");
		
		jQuery("#insertWskillsList").html("");
		jQuery("#insertLanguagesList").html("");
		jQuery("#cvnameloaded").html("");
	}

	jQuery('#degreetype').val('');
	jQuery('#degreetitle').val('');
	jQuery('#degreegrade').val('');
	jQuery('#institutionfrom').val('');
	jQuery('#institutionto').val('');
	jQuery('#institution').val('');
	jQuery('#institutiondescription').val('');

	jQuery('#wxcompanyname').val('');
	jQuery('#wxjobposition').val('');
	jQuery('#wxfrom').val('');
	jQuery('#wxto').val('');
	jQuery('#wxdescription').val('');

	jQuery('#publicationtitle').val('');
	jQuery('#publicationdate').val('');
	jQuery('#publicationlink').val('');
	jQuery('#publicationdescription').val('');
	
	jQuery('#certificationtitle').val('');
	jQuery('#certificationdate').val('');
	jQuery('#certificationinstitution').val('');
	jQuery('#certificationdescription').val('');
	
	jQuery('#qualificationtitle').val('');
	jQuery('#qualificationdate').val('');
	jQuery('#qualificationinstitution').val('');
	jQuery('#qualificationdescription').val('');
	
	jQuery('#awardtitle').val('');
	jQuery('#awarddate').val('');
	jQuery('#awardinstitution').val('');
	jQuery('#awarddescription').val('');
 
	jQuery('#languagename').val('');
	jQuery('#languagelevel').val('');
	jQuery('#langcertname').val('');
	jQuery('#langcertgrade').val('');

	jQuery('#wstype').val('');
	jQuery('#skillname').val('');

}

function scrollToTop()
{
	jQuery('html, body').animate({ scrollTop: jQuery('.cvmakerloader').offset().top - 150 }, 'slow');
}

jQuery(function(){
	
	var divid = 0;
	
	jQuery('#cvsavename').bind('keypress change focus blur', function (event) {
    var regex = new RegExp("^[a-zA-Z0-9\b_]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
       event.preventDefault();
       return false;
    }
	});
	
	jQuery('#cv-save').show();
	jQuery('#cv-content').hide();
	jQuery('#cv-layout').hide();
	
	jQuery('#btn-cv-save').on('click', function(){
		jQuery('#cv-save').show();
		jQuery('#cv-content').hide();
		jQuery('#cv-layout').hide();
		
		jQuery('#btn-cv-save').addClass('cvtabbtnpressed');
		jQuery('#btn-cv-save').removeClass('cvtabbtn');
		
		jQuery('#btn-cv-content').addClass('cvtabbtn');
		jQuery('#btn-cv-content').removeClass('cvtabbtnpressed');
		
		jQuery('#btn-cv-layout').addClass('cvtabbtn');
		jQuery('#btn-cv-layout').removeClass('cvtabbtnpressed');
	});
	
	jQuery('#btn-cv-content').on('click', function(){
		jQuery('#cv-save').hide();
		jQuery('#cv-content').show();
		jQuery('#cv-layout').hide();
		
		jQuery('#btn-cv-save').removeClass('cvtabbtnpressed');
		jQuery('#btn-cv-save').addClass('cvtabbtn');
		jQuery('#btn-cv-content').removeClass('cvtabbtn');
		jQuery('#btn-cv-content').addClass('cvtabbtnpressed');
		jQuery('#btn-cv-layout').removeClass('cvtabbtnpressed');
		jQuery('#btn-cv-layout').addClass('cvtabbtn');
		
	});
	
	jQuery('#btn-cv-layout').on('click', function(){
		jQuery('#cv-save').hide();
		jQuery('#cv-content').hide();
		jQuery('#cv-layout').show();
		
		jQuery('#btn-cv-save').removeClass('cvtabbtnpressed');
		jQuery('#btn-cv-save').addClass('cvtabbtn');
		jQuery('#btn-cv-content').removeClass('cvtabbtnpressed');
		jQuery('#btn-cv-content').addClass('cvtabbtn');
		jQuery('#btn-cv-layout').addClass('cvtabbtnpressed');
		jQuery('#btn-cv-layout').removeClass('cvtabbtn');
	});
	
	clearcvfields(true);
	
	jQuery('input[name="cvlayouts[]"]').on('change', function() {
			jQuery('input[name="' + this.name + '"]').not(this).prop('checked', false);
	});
	
	jQuery('#cvmaker-education').hide();
	jQuery('#cvmaker-experience').hide();
	jQuery('#cvmaker-publications').hide();
	
	jQuery('#cvmaker-certifications').hide();
	jQuery('#cvmaker-awards').hide();
	jQuery('#cvmaker-qualifications').hide();
	
	jQuery('#cvmaker-wskills').hide();
	jQuery('#cvmaker-languages').hide();
	jQuery('#cvmaker-activities').hide();
	jQuery("#editid").val("");
	
	jQuery('#contactinfobtn').on('click', function()
	{
		jQuery('#cvmaker-education').hide();
		jQuery('#cvmaker-experience').hide();
		jQuery('#cvmaker-publications').hide();
		jQuery('#cvmaker-wskills').hide();
		jQuery('#cvmaker-languages').hide();
		jQuery('#cvmaker-activities').hide();
		jQuery('#cvmaker-languages').hide();
		jQuery('#cvmaker-contactinfo').show();
		jQuery('#cvmaker-certifications').hide();
		jQuery('#cvmaker-awards').hide();
		jQuery('#cvmaker-qualifications').hide();
		jQuery("#editid").val("");
		scrollToTop();
		clearcvfields(false);
	});
	
	jQuery('#next1').on('click', function(){
		jQuery('#cvmaker-education').show();
		jQuery('#cvmaker-experience').hide();
		jQuery('#cvmaker-publications').hide();
		jQuery('#cvmaker-wskills').hide();
		jQuery('#cvmaker-languages').hide();
		jQuery('#cvmaker-activities').hide();
		jQuery('#cvmaker-languages').hide();
		jQuery('#cvmaker-contactinfo').hide();
		jQuery('#cvmaker-certifications').hide();
		jQuery('#cvmaker-awards').hide();
		jQuery('#cvmaker-qualifications').hide();
		jQuery("#editid").val("");
		scrollToTop();
		clearcvfields(false);
	});
	
	jQuery('#next2').on('click', function(){
		jQuery('#cvmaker-education').hide();
		jQuery('#cvmaker-experience').show();
		jQuery('#cvmaker-publications').hide();
		jQuery('#cvmaker-wskills').hide();
		jQuery('#cvmaker-languages').hide();
		jQuery('#cvmaker-activities').hide();
		jQuery('#cvmaker-languages').hide();
		jQuery('#cvmaker-contactinfo').hide();
		jQuery('#cvmaker-certifications').hide();
		jQuery('#cvmaker-awards').hide();
		jQuery('#cvmaker-qualifications').hide();
		jQuery("#editid").val("");
		scrollToTop();
		clearcvfields(false);
	});

	jQuery('#next3').on('click', function(){
		jQuery('#cvmaker-education').hide();
		jQuery('#cvmaker-experience').hide();
		jQuery('#cvmaker-publications').show();
		jQuery('#cvmaker-wskills').hide();
		jQuery('#cvmaker-languages').hide();
		jQuery('#cvmaker-activities').hide();
		jQuery('#cvmaker-languages').hide();
		jQuery('#cvmaker-contactinfo').hide();
		jQuery('#cvmaker-certifications').hide();
		jQuery('#cvmaker-awards').hide();
		jQuery('#cvmaker-qualifications').hide();
		jQuery("#editid").val("");
		scrollToTop();
		clearcvfields(false);
	});
	
	jQuery('#cvmakerbtn-certs').on('click', function(){
		jQuery('#cvmaker-education').hide();
		jQuery('#cvmaker-experience').hide();
		jQuery('#cvmaker-publications').hide();
		jQuery('#cvmaker-wskills').hide();
		jQuery('#cvmaker-languages').hide();
		jQuery('#cvmaker-activities').hide();
		jQuery('#cvmaker-languages').hide();
		jQuery('#cvmaker-contactinfo').hide();
		jQuery('#cvmaker-certifications').show();
		jQuery('#cvmaker-awards').hide();
		jQuery('#cvmaker-qualifications').hide();
		jQuery("#editid").val("");
		scrollToTop();
		clearcvfields(false);
	});

	jQuery('#cvmakerbtn-awards').on('click', function(){
		jQuery('#cvmaker-education').hide();
		jQuery('#cvmaker-experience').hide();
		jQuery('#cvmaker-publications').hide();
		jQuery('#cvmaker-wskills').hide();
		jQuery('#cvmaker-languages').hide();
		jQuery('#cvmaker-activities').hide();
		jQuery('#cvmaker-languages').hide();
		jQuery('#cvmaker-contactinfo').hide();
		jQuery('#cvmaker-certifications').hide();
		jQuery('#cvmaker-awards').show();
		jQuery('#cvmaker-qualifications').hide();
		jQuery("#editid").val("");
		scrollToTop();
		clearcvfields(false);
	});
	
	jQuery('#cvmakerbtn-qualifications').on('click', function(){
		jQuery('#cvmaker-education').hide();
		jQuery('#cvmaker-experience').hide();
		jQuery('#cvmaker-publications').hide();
		jQuery('#cvmaker-wskills').hide();
		jQuery('#cvmaker-languages').hide();
		jQuery('#cvmaker-activities').hide();
		jQuery('#cvmaker-languages').hide();
		jQuery('#cvmaker-contactinfo').hide();
		jQuery('#cvmaker-certifications').hide();
		jQuery('#cvmaker-awards').hide();
		jQuery('#cvmaker-qualifications').show();
		jQuery("#editid").val("");
		scrollToTop();
		clearcvfields(false);
	});

	jQuery('#next4').on('click', function(){
		jQuery('#cvmaker-education').hide();
		jQuery('#cvmaker-experience').hide();
		jQuery('#cvmaker-publications').hide();
		jQuery('#cvmaker-wskills').show();
		jQuery('#cvmaker-languages').hide();
		jQuery('#cvmaker-activities').hide();
		jQuery('#cvmaker-languages').hide();
		jQuery('#cvmaker-contactinfo').hide();
		jQuery('#cvmaker-certifications').hide();
		jQuery('#cvmaker-awards').hide();
		jQuery('#cvmaker-qualifications').hide();
		jQuery("#editid").val("");
		scrollToTop();
		clearcvfields(false);
	});

	jQuery('#next5').on('click', function(){
		jQuery('#cvmaker-education').hide();
		jQuery('#cvmaker-experience').hide();
		jQuery('#cvmaker-publications').hide();
		jQuery('#cvmaker-wskills').hide();
		jQuery('#cvmaker-languages').hide();
		jQuery('#cvmaker-activities').hide();
		jQuery('#cvmaker-certifications').hide();
		jQuery('#cvmaker-awards').hide();
		jQuery('#cvmaker-qualifications').hide();
		jQuery('#cvmaker-languages').show();
		jQuery('#cvmaker-contactinfo').hide();
		jQuery("#editid").val("");
		scrollToTop();
		clearcvfields(false);
	});

	jQuery('#next6').on('click', function(){
		jQuery('#cvmaker-education').hide();
		jQuery('#cvmaker-experience').hide();
		jQuery('#cvmaker-publications').hide();
		jQuery('#cvmaker-wskills').hide();
		jQuery('#cvmaker-languages').hide();
		jQuery('#cvmaker-activities').show();
		jQuery('#cvmaker-languages').hide();
		jQuery('#cvmaker-contactinfo').hide();
		jQuery('#cvmaker-certifications').hide();
		jQuery('#cvmaker-awards').hide();
		jQuery('#cvmaker-qualifications').hide();
		jQuery("#editid").val("");
		scrollToTop();
		clearcvfields(false);
	});
	
	jQuery('#addeducation').on('click', function(){
		if(jQuery("#editid").val() == "")
		{
			jQuery('#insertEducationList').append("<div id='cvmakercon-"+ divid +"'><div class='cvmakerlistitem' id='cvmaker-"+ divid +"'>" + jQuery('#institution').val() + "~" + jQuery('#degreetype').val() + "~" + jQuery('#degreetitle').val() + "~" + jQuery('#degreegrade').val() + "~" + jQuery('#institutionfrom').val() + "~" + jQuery('#institutionto').val() + "~" + jQuery('#institutiondescription').val() + " </div><div class='deletecontainer'><button class='cvmaker-button editbutton' id='editeducation-"+ divid +"'> <span class='fa fa-pencil-square-o fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='education-up-cvmaker-"+ divid +"'> <span class='fa fa-arrow-up fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='education-down-cvmaker-"+ divid +"'> <span class='fa fa-arrow-down fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button deletebutton' id='deletecvmaker-"+ divid +"'> <span class='fa fa-trash fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button></div></div>");
			divid++; 
			clearcvfields(false);
		}
		else
		{
			var idtoedit = jQuery("#editid").val();
			jQuery('#insertEducationList > #cvmakercon-' + idtoedit).html("<div class='cvmakerlistitem' id='cvmaker-"+ idtoedit +"'>" + jQuery('#institution').val() + "~" + jQuery('#degreetype').val() + "~" + jQuery('#degreetitle').val() + "~" + jQuery('#degreegrade').val() + "~" + jQuery('#institutionfrom').val() + "~" + jQuery('#institutionto').val() + "~" + jQuery('#institutiondescription').val() + "</div><div class='deletecontainer'><button class='cvmaker-button editbutton' id='editeducation-"+ idtoedit +"'> <span class='fa fa-pencil-square-o fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='education-up-cvmaker-"+ idtoedit +"'> <span class='fa fa-arrow-up fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='education-down-cvmaker-"+ idtoedit +"'> <span class='fa fa-arrow-down fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button deletebutton' id='deletecvmaker-"+ idtoedit +"'> <span class='fa fa-trash fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button></div>");
		}
	});
	
	jQuery('#addwx').on('click', function(){
		if(jQuery("#editid").val() == "")
		{
			jQuery('#insertWorkExList').append("<div id='cvmakercon-"+ divid +"'><div class='cvmakerlistitem' id='cvmaker-"+ divid +"'>" + jQuery('#wxcompanyname').val() + "~" + jQuery('#wxjobposition').val() + "~" + jQuery('#wxfrom').val() + "~" + jQuery('#wxto').val() + "~" + jQuery('#wxdescription').val() + " </div><div class='deletecontainer'><button class='cvmaker-button editbutton' id='editworkex-"+ divid +"'> <span class='fa fa-pencil-square-o fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='workex-up-cvmaker-"+ divid +"'> <span class='fa fa-arrow-up fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='workex-down-cvmaker-"+ divid +"'> <span class='fa fa-arrow-down fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button deletebutton' id='deletecvmaker-"+ divid +"'> <span class='fa fa-trash fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button></div><div>");
			divid++; 
			clearcvfields(false);
		}
		else
		{
			var idtoedit = jQuery("#editid").val();
			jQuery('#insertWorkExList > #cvmakercon-' + idtoedit).html("<div class='cvmakerlistitem' id='cvmaker-"+ idtoedit +"'>" + jQuery('#wxcompanyname').val() + "~" + jQuery('#wxjobposition').val() + "~" + jQuery('#wxfrom').val() + "~" + jQuery('#wxto').val() + "~" + jQuery('#wxdescription').val() + "</div><div class='deletecontainer'><button class='cvmaker-button editbutton' id='editworkex-"+ idtoedit +"'> <span class='fa fa-pencil-square-o fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='workex-up-cvmaker-"+ idtoedit +"'> <span class='fa fa-arrow-up fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='workex-down-cvmaker-"+ idtoedit +"'> <span class='fa fa-arrow-down fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button deletebutton' id='deletecvmaker-"+ idtoedit +"'> <span class='fa fa-trash fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button></div>");
		}
	});
	
	jQuery('#addpublications').on('click', function(){
		if(jQuery("#editid").val() == "")
		{
			jQuery('#insertPublicationsList').append("<div id='cvmakercon-"+ divid +"'><div class='cvmakerlistitem' id='cvmaker-"+ divid +"'>" + jQuery('#publicationtitle').val() + "~" + jQuery('#publicationdate').val() + "~" + jQuery('#publicationlink').val() + "~" + jQuery('#publicationdescription').val() + "</div><div class='deletecontainer'><button class='cvmaker-button editbutton' id='editpublications-"+ divid +"'> <span class='fa fa-pencil-square-o fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='publications-up-cvmaker-"+ divid +"'> <span class='fa fa-arrow-up fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='publications-down-cvmaker-"+ divid +"'> <span class='fa fa-arrow-down fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button deletebutton' id='deletecvmaker-"+ divid +"'> <span class='fa fa-trash fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button></div><div>");
			divid++; 
			clearcvfields(false);
		}
		else
		{
			var idtoedit = jQuery("#editid").val();
			jQuery('#insertPublicationsList > #cvmakercon-' + idtoedit).html("<div class='cvmakerlistitem' id='cvmaker-"+ idtoedit +"'>" + jQuery('#publicationtitle').val() + "~" + jQuery('#publicationdate').val() + "~" + jQuery('#publicationlink').val() + "~" + jQuery('#publicationdescription').val() + "</div><div class='deletecontainer'><button class='cvmaker-button editbutton' id='editpublications-"+ idtoedit +"'> <span class='fa fa-pencil-square-o fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='publications-up-cvmaker-"+ idtoedit +"'> <span class='fa fa-arrow-up fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='publications-down-cvmaker-"+ idtoedit +"'> <span class='fa fa-arrow-down fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button deletebutton' id='deletecvmaker-"+ idtoedit +"'> <span class='fa fa-trash fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button></div>");
		}
	});
	
	jQuery('#addcertification').on('click', function(){
		if(jQuery("#editid").val() == "")
		{
			jQuery('#insertCertificationsList').append("<div id='cvmakercon-"+ divid +"'><div class='cvmakerlistitem' id='cvmaker-"+ divid +"'>" + jQuery('#certificationtitle').val() + "~" + jQuery('#certificationdate').val() + "~" + jQuery('#certificationinstitution').val() + "~" + jQuery('#certificationdescription').val() + "</div><div class='deletecontainer'><button class='cvmaker-button editbutton' id='editcertifications-"+ divid +"'> <span class='fa fa-pencil-square-o fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='certifications-up-cvmaker-"+ divid +"'> <span class='fa fa-arrow-up fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='certifications-down-cvmaker-"+ divid +"'> <span class='fa fa-arrow-down fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button deletebutton' id='deletecvmaker-"+ divid +"'> <span class='fa fa-trash fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button></div><div>");
			divid++; 
			clearcvfields(false);
		}
		else
		{
			var idtoedit = jQuery("#editid").val();
			jQuery('#insertCertificationsList > #cvmakercon-' + idtoedit).html("<div class='cvmakerlistitem' id='cvmaker-"+ idtoedit +"'>" + jQuery('#certificationtitle').val() + "~" + jQuery('#certificationdate').val() + "~" + jQuery('#certificationinstitution').val() + "~" + jQuery('#certificationdescription').val() + "</div><div class='deletecontainer'><button class='cvmaker-button editbutton' id='editcertifications-"+ idtoedit +"'> <span class='fa fa-pencil-square-o fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='certifications-up-cvmaker-"+ idtoedit +"'> <span class='fa fa-arrow-up fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='certifications-down-cvmaker-"+ idtoedit +"'> <span class='fa fa-arrow-down fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button deletebutton' id='deletecvmaker-"+ idtoedit +"'> <span class='fa fa-trash fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button></div>");
		}
	});
	
	jQuery('#addaward').on('click', function(){
		if(jQuery("#editid").val() == "")
		{
			jQuery('#insertAwardsList').append("<div id='cvmakercon-"+ divid +"'><div class='cvmakerlistitem' id='cvmaker-"+ divid +"'>" + jQuery('#awardtitle').val() + "~" + jQuery('#awarddate').val() + "~" + jQuery('#awardinstitution').val() + "~" + jQuery('#awarddescription').val() + "</div><div class='deletecontainer'><button class='cvmaker-button editbutton' id='editawards-"+ divid +"'> <span class='fa fa-pencil-square-o fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='awards-up-cvmaker-"+ divid +"'> <span class='fa fa-arrow-up fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='awards-down-cvmaker-"+ divid +"'> <span class='fa fa-arrow-down fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button deletebutton' id='deletecvmaker-"+ divid +"'> <span class='fa fa-trash fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button></div><div>");
			divid++; 
			clearcvfields(false);
		}
		else
		{
			var idtoedit = jQuery("#editid").val();
			jQuery('#insertAwardsList > #cvmakercon-' + idtoedit).html("");
			jQuery('#insertAwardsList > #cvmakercon-' + idtoedit).html("<div class='cvmakerlistitem' id='cvmaker-"+ idtoedit +"'>" + jQuery('#awardtitle').val() + "~" + jQuery('#awarddate').val() + "~" + jQuery('#awardinstitution').val() + "~" + jQuery('#awarddescription').val() + "</div><div class='deletecontainer'><button class='cvmaker-button editbutton' id='editawards-"+ idtoedit +"'> <span class='fa fa-pencil-square-o fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='awards-up-cvmaker-"+ idtoedit +"'> <span class='fa fa-arrow-up fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='awards-down-cvmaker-"+ idtoedit +"'> <span class='fa fa-arrow-down fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button deletebutton' id='deletecvmaker-"+ idtoedit +"'> <span class='fa fa-trash fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button></div>");
			
		}
	});
	
	jQuery('#addqualification').on('click', function(){
		if(jQuery("#editid").val() == "")
		{
			jQuery('#insertQualificationsList').append("<div id='cvmakercon-"+ divid +"'><div class='cvmakerlistitem' id='cvmaker-"+ divid +"'>" + jQuery('#qualificationtitle').val() + "~" + jQuery('#qualificationdate').val() + "~" + jQuery('#qualificationinstitution').val() + "~" + jQuery('#qualificationdescription').val() + "</div><div class='deletecontainer'><button class='cvmaker-button editbutton' id='editqualifications-"+ divid +"'> <span class='fa fa-pencil-square-o fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='qualifications-up-cvmaker-"+ divid +"'> <span class='fa fa-arrow-up fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='qualifications-down-cvmaker-"+ divid +"'> <span class='fa fa-arrow-down fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button deletebutton' id='deletecvmaker-"+ divid +"'> <span class='fa fa-trash fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button></div><div>");
			divid++; 
			clearcvfields(false);
		}
		else
		{
			var idtoedit = jQuery("#editid").val();
			jQuery('#insertQualificationsList > #cvmakercon-' + idtoedit).html("<div class='cvmakerlistitem' id='cvmaker-"+ idtoedit +"'>" + jQuery('#qualificationtitle').val() + "~" + jQuery('#qualificationdate').val() + "~" + jQuery('#qualificationinstitution').val() + "~" + jQuery('#qualificationdescription').val() + "</div><div class='deletecontainer'><button class='cvmaker-button editbutton' id='editqualifications-"+ idtoedit +"'> <span class='fa fa-pencil-square-o fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='qualifications-up-cvmaker-"+ idtoedit +"'> <span class='fa fa-arrow-up fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='qualifications-down-cvmaker-"+ idtoedit +"'> <span class='fa fa-arrow-down fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button deletebutton' id='deletecvmaker-"+ idtoedit +"'> <span class='fa fa-trash fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button></div>");
		}
	});
	
	jQuery('#addlanguage').on('click', function(){
		if(jQuery("#editid").val() == "")
		{
			jQuery('#insertLanguagesList').append("<div id='cvmakercon-"+ divid +"'><div class='cvmakerlistitem' id='cvmaker-"+ divid +"'>" + jQuery('#languagename').val() + "~" + jQuery('#languagelevel').val() + "~" + jQuery('#langcertname').val() + "~" + jQuery('#langcertgrade').val() + "</div><div class='deletecontainer'><button class='cvmaker-button editbutton' id='editlanguages-"+ divid +"'> <span class='fa fa-pencil-square-o fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='languages-up-cvmaker-"+ divid +"'> <span class='fa fa-arrow-up fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='languages-down-cvmaker-"+ divid +"'> <span class='fa fa-arrow-down fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button deletebutton' id='deletecvmaker-"+ divid +"'> <span class='fa fa-trash fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button></div><div>");
			divid++; 
			clearcvfields(false);
		}
		else
		{
			var idtoedit = jQuery("#editid").val();
			jQuery('#insertLanguagesList > #cvmakercon-' + idtoedit).html("<div class='cvmakerlistitem' id='cvmaker-"+ idtoedit +"'>" + jQuery('#languagename').val() + "~" + jQuery('#languagelevel').val() + "~" + jQuery('#langcertname').val() + "~" + jQuery('#langcertgrade').val() + "</div><div class='deletecontainer'><button class='cvmaker-button editbutton' id='editlanguages-"+ idtoedit +"'> <span class='fa fa-pencil-square-o fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='languages-up-cvmaker-"+ idtoedit +"'> <span class='fa fa-arrow-up fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='languages-down-cvmaker-"+ idtoedit +"'> <span class='fa fa-arrow-down fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button deletebutton' id='deletecvmaker-"+ idtoedit +"'> <span class='fa fa-trash fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button></div>");
		}
	});
	
	jQuery('#addskill').on('click', function(){
		if(jQuery("#editid").val() == "")
		{
			jQuery('#insertWskillsList').append("<div id='cvmakercon-"+ divid +"'><div class='cvmakerlistitem' id='cvmaker-"+ divid +"'>" + jQuery('#wstype').val() + "~" + jQuery('#skillname').val() + "</div><div class='deletecontainer'><button class='cvmaker-button editbutton' id='editskills-"+ divid +"'> <span class='fa fa-pencil-square-o fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='skills-up-cvmaker-"+ divid +"'> <span class='fa fa-arrow-up fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='skills-down-cvmaker-"+ divid +"'> <span class='fa fa-arrow-down fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button deletebutton' id='deletecvmaker-"+ divid +"'> <span class='fa fa-trash fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button></div><div>");
			divid++; 
			clearcvfields(false);
		}
		else
		{
			var idtoedit = jQuery("#editid").val();
			jQuery('#insertWskillsList > #cvmakercon-' + idtoedit).html("<div class='cvmakerlistitem' id='cvmaker-"+ idtoedit +"'>" + jQuery('#wstype').val() + "~" + jQuery('#skillname').val() + "</div><div class='deletecontainer'><button class='cvmaker-button editbutton' id='editskills-"+ idtoedit +"'> <span class='fa fa-pencil-square-o fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='skills-up-cvmaker-"+ idtoedit +"'> <span class='fa fa-arrow-up fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='skills-down-cvmaker-"+ idtoedit +"'> <span class='fa fa-arrow-down fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button deletebutton' id='deletecvmaker-"+ idtoedit +"'> <span class='fa fa-trash fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button></div>");
		}
	});
	
	
	jQuery('.canceledit').on('click', function(){
		jQuery("#editid").val('');
		clearcvfields(false);
	});
	
	jQuery('#loadcv').on('click', function(){
		var r = confirm("Are you sure you want to load a CV?\nUnless saved, all your current changes will be discarded!\nAre you sure you want to continue?");
		if (r == false) 
			return;
		var cv = jQuery('#mycvs').val();
		jQuery('#loadcvid').val(cv);
		jQuery("#editid").val('');
		clearcvfields(true);

		jQuery("#cvsavename").prop( "disabled", true );
		cv2 = cv.split("]");
		jQuery("#cvnameloaded").html(cv2[0] + "]");
		
		jQuery.ajax({
			url: "<?php echo plugin_dir_url( __FILE__ );  ?>loadcv.php",
			dataType: 'json',
			method: "POST",
			data: { 
				token: jQuery('#token').val(),
				cv: cv
			},
			success: function(result){
				cv = cv.split("]");
				jQuery('#loadpanel').html("Working on " + cv[0] + "]");
				jQuery('#cvsavename').val(result["cvsavename"]);
				jQuery('#fullname').val(result["fullname"]);
				jQuery('#emailaddresses').val(result["emailaddress"]);
				jQuery('#phonenumbers').val(result["phonenumbers"]);
				jQuery('#websites').val(result["websites"]);
				jQuery('#adline1').val(result["adline1"]);
				jQuery('#adline2').val(result["adline2"]);
				jQuery('#adline3').val(result["adline3"]);
				jQuery('#jobtitle').val(result["jobtitle"]);
				jQuery('#birthdate').val(result["birthdate"]);
				jQuery('#nationality').val(result["nationality"]);
				jQuery('#contactdescription').val(result["contactdescription"]);
				jQuery('#hobbies').val(result["hobbies"]);
				
				jQuery('#insertEducationList').html("");
				for(var i = 0; i < result["education"].length; i++)
				{
					jQuery('#insertEducationList').append("<div id='cvmakercon-"+ divid +"'><div class='cvmakerlistitem' id='cvmaker-"+ divid +"'>" + result["education"][i][0] + "~" + result["education"][i][1] + "~" 	+ result["education"][i][2] + "~" + result["education"][i][3] + "~" + result["education"][i][4] + "~" + result["education"][i][5] + "~" + result["education"][i][6] + " </div><div class='deletecontainer'><button class='cvmaker-button editbutton' id='editeducation-"+ divid +"'> <span class='fa fa-pencil-square-o fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='education-up-cvmaker-"+ divid +"'> <span class='fa fa-arrow-up fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='education-down-cvmaker-"+ divid +"'> <span class='fa fa-arrow-down fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button deletebutton' id='deletecvmaker-"+ divid +"'> <span class='fa fa-trash fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button></div><div>");
					divid++; 
				}
				
				jQuery('#insertWorkExList').html("");
				for(var i = 0; i < result["workex"].length; i++)
				{
					jQuery('#insertWorkExList').append("<div id='cvmakercon-"+ divid +"'><div class='cvmakerlistitem' id='cvmaker-"+ divid +"'>" + result["workex"][i][0] + "~" + result["workex"][i][1] + "~" + result["workex"][i][2] + "~" + result["workex"][i][3] + "~" + result["workex"][i][4] + " </div><div class='deletecontainer'><button class='cvmaker-button editbutton' id='editworkex-"+ divid +"'> <span class='fa fa-pencil-square-o fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='workex-up-cvmaker-"+ divid +"'> <span class='fa fa-arrow-up fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='workex-down-cvmaker-"+ divid +"'> <span class='fa fa-arrow-down fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button deletebutton' id='deletecvmaker-"+ divid +"'> <span class='fa fa-trash fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button></div><div>");
					divid++; 
				}
				
				jQuery('#insertPublicationsList').html("");
				for(var i = 0; i < result["publications"].length; i++)
				{
					jQuery('#insertPublicationsList').append("<div id='cvmakercon-"+ divid +"'><div class='cvmakerlistitem' id='cvmaker-"+ divid +"'>" + result["publications"][i][0] + "~" + result["publications"][i][1] + "~" + result["publications"][i][2] + "~" + result["publications"][i][3] + "</div><div class='deletecontainer'><button class='cvmaker-button editbutton' id='editpublications-"+ divid +"'> <span class='fa fa-pencil-square-o fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='publications-up-cvmaker-"+ divid +"'> <span class='fa fa-arrow-up fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='publications-down-cvmaker-"+ divid +"'> <span class='fa fa-arrow-down fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button deletebutton' id='deletecvmaker-"+ divid +"'> <span class='fa fa-trash fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button></div><div>");
					divid++; 
				}
				
				jQuery('#insertCertificationsList').html("");
				for(var i = 0; i < result["certifications"].length; i++)
				{
					jQuery('#insertCertificationsList').append("<div id='cvmakercon-"+ divid +"'><div class='cvmakerlistitem' id='cvmaker-"+ divid +"'>" + result["certifications"][i][0] + "~" + result["certifications"][i][1] + "~" + result["certifications"][i][2] + "~" + result["certifications"][i][3] + "</div><div class='deletecontainer'><button class='cvmaker-button editbutton' id='editcertifications-"+ divid +"'> <span class='fa fa-pencil-square-o fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='certifications-up-cvmaker-"+ divid +"'> <span class='fa fa-arrow-up fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='certifications-down-cvmaker-"+ divid +"'> <span class='fa fa-arrow-down fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button deletebutton' id='deletecvmaker-"+ divid +"'> <span class='fa fa-trash fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button></div><div>");
					divid++; 
				}
				
				jQuery('#insertAwardsList').html("");
				for(var i = 0; i < result["awards"].length; i++)
				{
					jQuery('#insertAwardsList').append("<div id='cvmakercon-"+ divid +"'><div class='cvmakerlistitem' id='cvmaker-"+ divid +"'>" + result["awards"][i][0] + "~" + result["awards"][i][1] + "~" + result["awards"][i][2] + "~" + result["awards"][i][3] + "</div><div class='deletecontainer'><button class='cvmaker-button editbutton' id='editawards-"+ divid +"'> <span class='fa fa-pencil-square-o fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='awards-up-cvmaker-"+ divid +"'> <span class='fa fa-arrow-up fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='awards-down-cvmaker-"+ divid +"'> <span class='fa fa-arrow-down fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button deletebutton' id='deletecvmaker-"+ divid +"'> <span class='fa fa-trash fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button></div><div>");
					divid++; 
				}
				
				jQuery('#insertQualificationsList').html("");
				for(var i = 0; i < result["qualifications"].length; i++)
				{
					jQuery('#insertQualificationsList').append("<div id='cvmakercon-"+ divid +"'><div class='cvmakerlistitem' id='cvmaker-"+ divid +"'>" + result["qualifications"][i][0] + "~" + result["qualifications"][i][1] + "~" + result["qualifications"][i][2] + "~" + result["qualifications"][i][3] + "</div><div class='deletecontainer'><button class='cvmaker-button editbutton' id='editqualifications-"+ divid +"'> <span class='fa fa-pencil-square-o fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='qualifications-up-cvmaker-"+ divid +"'> <span class='fa fa-arrow-up fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='qualifications-down-cvmaker-"+ divid +"'> <span class='fa fa-arrow-down fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button deletebutton' id='deletecvmaker-"+ divid +"'> <span class='fa fa-trash fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button></div><div>");
					divid++; 
				}
				
				jQuery('#insertLanguagesList').html("");
				for(var i = 0; i < result["languages"].length; i++)
				{
					jQuery('#insertLanguagesList').append("<div id='cvmakercon-"+ divid +"'><div class='cvmakerlistitem' id='cvmaker-"+ divid +"'>" + result["languages"][i][0] + "~" + result["languages"][i][1] + "~" + result["languages"][i][2] + "~" + result["languages"][i][3] + "</div><div class='deletecontainer'><button class='cvmaker-button editbutton' id='editlanguages-"+ divid +"'> <span class='fa fa-pencil-square-o fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='languages-up-cvmaker-"+ divid +"'> <span class='fa fa-arrow-up fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='languages-down-cvmaker-"+ divid +"'> <span class='fa fa-arrow-down fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button deletebutton' id='deletecvmaker-"+ divid +"'> <span class='fa fa-trash fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button></div><div>");
					divid++; 
				}
				
				jQuery('#insertWskillsList').html("");
				for(var i = 0; i < result["skills"].length; i++)
				{
					jQuery('#insertWskillsList').append("<div id='cvmakercon-"+ divid +"'><div class='cvmakerlistitem' id='cvmaker-"+ divid +"'>" + result["skills"][i][0] + "~" + result["skills"][i][1] + "</div><div class='deletecontainer'><button class='cvmaker-button editbutton' id='editskills-"+ divid +"'> <span class='fa fa-pencil-square-o fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='skills-up-cvmaker-"+ divid +"'> <span class='fa fa-arrow-up fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='skills-down-cvmaker-"+ divid +"'> <span class='fa fa-arrow-down fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button deletebutton' id='deletecvmaker-"+ divid +"'> <span class='fa fa-trash fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button></div><div>");
					divid++; 
				}
				
			},
			error: function(result){
				alert('Uknown Error!');
			}
		});
	});
	
	jQuery(document).on("click", ".movebutton", function(){
		var localid = jQuery(this).prop('id');
		var containerid = localid.split("-"); 
		//alert("Category:" + containerid[0] + ", Move:" + containerid[1] + ", id:" + containerid[3]);
		if(containerid[1] == "down")
		{
			if (jQuery("#cvmakercon-" + containerid[3]).next().length) 
			{
				var next = jQuery("#cvmakercon-" + containerid[3]).next();
				jQuery("#cvmakercon-" + containerid[3]).insertAfter(next);
			}
		}
		else
		{
			if (jQuery("#cvmakercon-" + containerid[3]).prev().length) 
			{
			 	var prev = jQuery("#cvmakercon-" + containerid[3]).prev();
				jQuery("#cvmakercon-" + containerid[3]).insertBefore(prev);
			}
		}
	});
	
	jQuery('#deletecv').on('click', function(){
		var r = confirm("Are you sure you want to delete your CV?\nUnless saved locally, your CV will be deleted permanently!\nAre you sure you want to continue?");
		if (r == true) 
		{
			var cv = jQuery('#mycvs').val();
			jQuery('#loadcvid').val('');
			jQuery("#cvsavename").prop( "disabled", false );
			jQuery('#loadpanel').html("");
			
			clearcvfields(true);
			
			jQuery.ajax({
				url: "<?php echo plugin_dir_url( __FILE__ );  ?>deletecv.php",
				dataType: 'html',
				method: "POST",
				data: { 
					token: jQuery('#token').val(),
					cv: cv
				},
				success: function(result){
					cv = cv.split("]");
					alert("CV '" + cv[0] + "]' successfully deleted!");
					location.reload(true);
				},
				error: function(result){
					alert('Uknown Error!');
				}
			});
		}
	});
	
	jQuery('#newcv').on('click', function(){
		var r = confirm("Are you sure you want to start a new CV?\nUnless saved, all your current changes will be discarded!\nAre you sure you want to continue?");
		if (r == true) 
		{
			jQuery('#loadcvid').val('');
			jQuery('#loadpanel').html("");
			jQuery("#editid").val('');
			
			jQuery("#cvsavename").prop( "disabled", false );
			
			jQuery('#cv-save').hide();
			jQuery('#cv-content').show();
			jQuery('#btn-cv-content').click();
			
			clearcvfields(true);
		}
	});
	
	jQuery(document).on("click", ".deletebutton", function(){
		var localid = jQuery(this).prop('id');
		jQuery("#editid").val('');
		var containerid = localid.split("-"); 
		jQuery("#cvmakercon-" + containerid[1]).remove();
	});
	
	
	jQuery('#loadfilecv').on("click", function(){
		var fd = new FormData();    
		fd.append( 'filecv', document.getElementById('filecv').files[0]);
		fd.append( 'token', jQuery("#token").val());
		jQuery.ajax({
			url: '<?php echo plugin_dir_url( __FILE__ );  ?>loadfilecv.php',
			type: 'POST',
			data: fd,
			processData: false,
			contentType: false,
			success: function(result){
				//alert(result + " ==> " + result['fullname']);
				var r = confirm("Are you sure you want to load a CV?\nUnless saved, all your current changes will be discarded!\nAre you sure you want to continue?");
				if (r == false) 
					return;
				
				jQuery("#editid").val('');
				jQuery('#loadcvid').val('');
				jQuery('#loadpanel').html("Info taken from a local file you uploaded BUT it is not saved in our system!");
				clearcvfields(true);
				jQuery("#cvsavename").prop( "disabled", false );
			
				jQuery('#cvsavename').val(result["cvsavename"]);
				jQuery('#fullname').val(result["fullname"]);
				jQuery('#emailaddresses').val(result["emailaddress"]);
				jQuery('#phonenumbers').val(result["phonenumbers"]);
				jQuery('#websites').val(result["websites"]);
				jQuery('#adline1').val(result["adline1"]);
				jQuery('#adline2').val(result["adline2"]);
				jQuery('#adline3').val(result["adline3"]);
				jQuery('#nationality').val(result["nationality"]);
				jQuery('#contactdescription').val(result["contactdescription"]);
				jQuery('#hobbies').val(result["hobbies"]);
				jQuery('#jobtitle').val(result["jobtitle"]);
				jQuery('#birthdate').val(result["birthdate"]);
				
				jQuery('#insertEducationList').html("");
				for(var i = 0; i < result["education"].length; i++)
				{
					jQuery('#insertEducationList').append("<div id='cvmakercon-"+ divid +"'><div class='cvmakerlistitem' id='cvmaker-"+ divid +"'>" + result["education"][i][0] + "~" + result["education"][i][1] + "~" 	+ result["education"][i][2] + "~" + result["education"][i][3] + "~" + result["education"][i][4] + "~" + result["education"][i][5] + "~" + result["education"][i][6] + " </div><div class='deletecontainer'><button class='cvmaker-button editbutton' id='editeducation-"+ divid +"'> <span class='fa fa-pencil-square-o fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='education-up-cvmaker-"+ divid +"'> <span class='fa fa-arrow-up fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='education-down-cvmaker-"+ divid +"'> <span class='fa fa-arrow-down fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button deletebutton' id='deletecvmaker-"+ divid +"'> <span class='fa fa-trash fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button></div><div>");
					divid++; 
				}
				
				jQuery('#insertWorkExList').html("");
				for(var i = 0; i < result["workex"].length; i++)
				{
					jQuery('#insertWorkExList').append("<div id='cvmakercon-"+ divid +"'><div class='cvmakerlistitem' id='cvmaker-"+ divid +"'>" + result["workex"][i][0] + "~" + result["workex"][i][1] + "~" + result["workex"][i][2] + "~" + result["workex"][i][3] + "~" + result["workex"][i][4] + " </div><div class='deletecontainer'><button class='cvmaker-button editbutton' id='editworkex-"+ divid +"'> <span class='fa fa-pencil-square-o fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='workex-up-cvmaker-"+ divid +"'> <span class='fa fa-arrow-up fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='workex-down-cvmaker-"+ divid +"'> <span class='fa fa-arrow-down fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button deletebutton' id='deletecvmaker-"+ divid +"'> <span class='fa fa-trash fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button></div><div>");
					divid++; 
				}
				
				jQuery('#insertPublicationsList').html("");
				for(var i = 0; i < result["publications"].length; i++)
				{
					jQuery('#insertPublicationsList').append("<div id='cvmakercon-"+ divid +"'><div class='cvmakerlistitem' id='cvmaker-"+ divid +"'>" + result["publications"][i][0] + "~" + result["publications"][i][1] + "~" + result["publications"][i][2] + "~" + result["publications"][i][3] + "</div><div class='deletecontainer'><button class='cvmaker-button editbutton' id='editpublications-"+ divid +"'> <span class='fa fa-pencil-square-o fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='publications-up-cvmaker-"+ divid +"'> <span class='fa fa-arrow-up fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='publications-down-cvmaker-"+ divid +"'> <span class='fa fa-arrow-down fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button deletebutton' id='deletecvmaker-"+ divid +"'> <span class='fa fa-trash fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button></div><div>");
					divid++; 
				}
				
				jQuery('#insertCertificationsList').html("");
				for(var i = 0; i < result["certifications"].length; i++)
				{
					jQuery('#insertCertificationsList').append("<div id='cvmakercon-"+ divid +"'><div class='cvmakerlistitem' id='cvmaker-"+ divid +"'>" + result["certifications"][i][0] + "~" + result["certifications"][i][1] + "~" + result["certifications"][i][2] + "~" + result["certifications"][i][3] + "</div><div class='deletecontainer'><button class='cvmaker-button editbutton' id='editcertifications-"+ divid +"'> <span class='fa fa-pencil-square-o fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='certifications-up-cvmaker-"+ divid +"'> <span class='fa fa-arrow-up fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='certifications-down-cvmaker-"+ divid +"'> <span class='fa fa-arrow-down fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button deletebutton' id='deletecvmaker-"+ divid +"'> <span class='fa fa-trash fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button></div><div>");
					divid++; 
				}
				
				jQuery('#insertAwardsList').html("");
				for(var i = 0; i < result["awards"].length; i++)
				{
					jQuery('#insertAwardsList').append("<div id='cvmakercon-"+ divid +"'><div class='cvmakerlistitem' id='cvmaker-"+ divid +"'>" + result["awards"][i][0] + "~" + result["awards"][i][1] + "~" + result["awards"][i][2] + "~" + result["awards"][i][3] + "</div><div class='deletecontainer'><button class='cvmaker-button editbutton' id='editawards-"+ divid +"'> <span class='fa fa-pencil-square-o fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='awards-up-cvmaker-"+ divid +"'> <span class='fa fa-arrow-up fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='awards-down-cvmaker-"+ divid +"'> <span class='fa fa-arrow-down fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button deletebutton' id='deletecvmaker-"+ divid +"'> <span class='fa fa-trash fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button></div><div>");
					divid++; 
				}
				
				jQuery('#insertQualificationsList').html("");
				for(var i = 0; i < result["qualifications"].length; i++)
				{
					jQuery('#insertQualificationsList').append("<div id='cvmakercon-"+ divid +"'><div class='cvmakerlistitem' id='cvmaker-"+ divid +"'>" + result["qualifications"][i][0] + "~" + result["qualifications"][i][1] + "~" + result["qualifications"][i][2] + "~" + result["qualifications"][i][3] + "</div><div class='deletecontainer'><button class='cvmaker-button editbutton' id='editqualifications-"+ divid +"'> <span class='fa fa-pencil-square-o fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='qualifications-up-cvmaker-"+ divid +"'> <span class='fa fa-arrow-up fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='qualifications-down-cvmaker-"+ divid +"'> <span class='fa fa-arrow-down fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button deletebutton' id='deletecvmaker-"+ divid +"'> <span class='fa fa-trash fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button></div><div>");
					divid++; 
				}
				
				jQuery('#insertLanguagesList').html("");
				for(var i = 0; i < result["languages"].length; i++)
				{
					jQuery('#insertLanguagesList').append("<div id='cvmakercon-"+ divid +"'><div class='cvmakerlistitem' id='cvmaker-"+ divid +"'>" + result["languages"][i][0] + "~" + result["languages"][i][1] + "~" + result["languages"][i][2] + "~" + result["languages"][i][3] + "</div><div class='deletecontainer'><button class='cvmaker-button editbutton' id='editlanguages-"+ divid +"'> <span class='fa fa-pencil-square-o fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='languages-up-cvmaker-"+ divid +"'> <span class='fa fa-arrow-up fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='languages-down-cvmaker-"+ divid +"'> <span class='fa fa-arrow-down fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button deletebutton' id='deletecvmaker-"+ divid +"'> <span class='fa fa-trash fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button></div><div>");
					divid++; 
				}
				
				jQuery('#insertWskillsList').html("");
				for(var i = 0; i < result["skills"].length; i++)
				{
					jQuery('#insertWskillsList').append("<div id='cvmakercon-"+ divid +"'><div class='cvmakerlistitem' id='cvmaker-"+ divid +"'>" + result["skills"][i][0] + "~" + result["skills"][i][1] + "</div><div class='deletecontainer'><button class='cvmaker-button editbutton' id='editskills-"+ divid +"'> <span class='fa fa-pencil-square-o fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='skills-up-cvmaker-"+ divid +"'> <span class='fa fa-arrow-up fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button movebutton' id='skills-down-cvmaker-"+ divid +"'> <span class='fa fa-arrow-down fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button><button class='cvmaker-button deletebutton' id='deletecvmaker-"+ divid +"'> <span class='fa fa-trash fa-fw' style='font-size: 11px; width: 10px; height: 10px;'></span> </button></div><div>");
					divid++; 
				}
				
				jQuery('#cv-save').hide();
				jQuery('#cv-content').show();
				jQuery('#btn-cv-content').click();
			},
			error: function(result){
				alert('Error while uploading your CV!\nCheck that your file\'s content is in json format!');
			}
		});     
	});
	
	jQuery(document).on("click", ".editbutton", function(){
		var localid = jQuery(this).prop('id');
		var containerid = localid.split("-");
		var category = containerid[0].replace("edit", "");
		jQuery("#editid").val(containerid[1]);
		var data = "";
		//alert("Edit(" + category + ") "+ containerid[1] + " - TODO");
		if(category == "education")
		{
			data = jQuery("#insertEducationList > div > #cvmaker-" + containerid[1]).html();
			data = data.split("~");
			
			jQuery('#institution').val(data[0]);
			jQuery('#degreetype').val(data[1]);
			jQuery('#degreetitle').val(data[2]);
			jQuery('#degreegrade').val(data[3]);
			jQuery('#institutionfrom').val(data[4]);
			jQuery('#institutionto').val(data[5]);
			jQuery('#institutiondescription').val(data[6]);
		}
		else if( category== "workex")
		{
			data = jQuery("#insertWorkExList > div > #cvmaker-" + containerid[1]).html();
			data = data.split("~");
			
			jQuery('#wxcompanyname').val(data[0]);
			jQuery('#wxjobposition').val(data[1]);
			jQuery('#wxfrom').val(data[2]);
			jQuery('#wxto').val(data[3]);
			jQuery('#wxdescription').val(data[4]);
		}
		else if( category== "publications")
		{
			data = jQuery("#insertPublicationsList > div > #cvmaker-" + containerid[1]).html();
			data = data.split("~");
			
			jQuery('#publicationtitle').val(data[0]);
			jQuery('#publicationdate').val(data[1]);
			jQuery('#publicationlink').val(data[2]);
			jQuery('#publicationdescription').val(data[3]);
		}
		else if( category== "certifications")
		{
			data = jQuery("#insertCertificationsList > div > #cvmaker-" + containerid[1]).html();
			data = data.split("~");
			
			jQuery('#certificationtitle').val(data[0]);
			jQuery('#certificationdate').val(data[1]);
			jQuery('#certificationinstitution').val(data[2]);
			jQuery('#certificationdescription').val(data[3]);
		}
		else if( category== "awards")
		{
			data = jQuery("#insertAwardsList > div > #cvmaker-" + containerid[1]).html();
			data = data.split("~");
			
			jQuery('#awardtitle').val(data[0]);
			jQuery('#awarddate').val(data[1]);
			jQuery('#awardinstitution').val(data[2]);
			jQuery('#awarddescription').val(data[3]);
		}
		else if( category== "qualifications")
		{
			data = jQuery("#insertQualificationsList > div > #cvmaker-" + containerid[1]).html();
			data = data.split("~");
			
			jQuery('#qualificationtitle').val(data[0]);
			jQuery('#qualificationdate').val(data[1]);
			jQuery('#qualificationinstitution').val(data[2]);
			jQuery('#qualificationdescription').val(data[3]);
		}
		else if( category== "languages")
		{
			data = jQuery("#insertLanguagesList > div > #cvmaker-" + containerid[1]).html();
			data = data.split("~");
			
			jQuery('#languagename').val(data[0]);
			jQuery('#languagelevel').val(data[1]);
			jQuery('#langcertname').val(data[2]);
			jQuery('#langcertgrade').val(data[3]);
		}
		else if( category== "skills")
		{
			data = jQuery("#insertWskillsList > div > #cvmaker-" + containerid[1]).html();
			data = data.split("~");
			jQuery('#wstype').val(data[0]);
			jQuery('#skillname').val(data[1]);
		}
	});
	
});

function isValidEmailAddress(emailAddress) {
	var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
	return pattern.test(emailAddress);
};

function actionsCV(cvaction, cvtype) {

	var cvlayout = jQuery("input[name='cvlayouts[]']:checked").val();

	var education = new Array();
	jQuery.each(jQuery("#insertEducationList > div > .cvmakerlistitem"), function() {
		education.push(jQuery(this).html());
	});
	
	var workex = new Array();
	jQuery.each(jQuery("#insertWorkExList > div > .cvmakerlistitem"), function() {
		workex.push(jQuery(this).html());
	});
	
	var publications = new Array();
	jQuery.each(jQuery("#insertPublicationsList > div > .cvmakerlistitem"), function() {
		publications.push(jQuery(this).html());
	});
	
	var certifications = new Array();
	jQuery.each(jQuery("#insertCertificationsList > div > .cvmakerlistitem"), function() {
		certifications.push(jQuery(this).html());
	});
	
	var awards = new Array();
	jQuery.each(jQuery("#insertAwardsList > div > .cvmakerlistitem"), function() {
		awards.push(jQuery(this).html());
	});
	
	var qualifications = new Array();
	jQuery.each(jQuery("#insertQualificationsList > div > .cvmakerlistitem"), function() {
		qualifications.push(jQuery(this).html());
	});
	
	var skills = new Array();
	jQuery.each(jQuery("#insertWskillsList > div > .cvmakerlistitem"), function() {
		skills.push(jQuery(this).html());
	});
	
	var languages = new Array();
	jQuery.each(jQuery("#insertLanguagesList > div > .cvmakerlistitem"), function() {
		languages.push(jQuery(this).html());
	});
	
	jQuery.ajax({
		url: "<?php echo plugin_dir_url( __FILE__ );  ?>createcv.php",
		dataType : "HTML",
		method: "POST",
		data: { 
			token: jQuery('#token').val(),
			cvsavename: jQuery('#cvsavename').val(),
			fullname: jQuery('#fullname').val(),
			emailaddress: jQuery('#emailaddresses').val(),
			phonenumbers: jQuery('#phonenumbers').val(),
			websites: jQuery('#websites').val(),
			adline1: jQuery('#adline1').val(),
			adline2: jQuery('#adline2').val(),
			adline3: jQuery('#adline3').val(),
			nationality: jQuery('#nationality').val(),
			contactdescription: jQuery('#contactdescription').val(),
			jobtitle: jQuery("#jobtitle").val(),
			birthdate: jQuery("#birthdate").val(),
			hobbies: jQuery('#hobbies').val(),
			education: education,
			awards: awards,
			certifications: certifications,
			qualifications: qualifications,
			workex: workex,
			publications: publications,
			loadcvid: jQuery('#loadcvid').val(),
			skills: skills,
			languages: languages,
			cvlayout: cvlayout,
			cvaction: cvaction,
			cvtype: cvtype,
			username: "<?php if(is_user_logged_in()){echo $current_user->user_login;}else{echo "NONE";} ?>",
			userid: "<?php if(is_user_logged_in()){echo $current_user->ID;}else{echo "NONE";} ?>"
		},
		success: function(result){
			if(cvaction == 'download' && cvtype == 'json')
			{
				var url = 'data:text/json;charset=utf8,' + encodeURIComponent(result);
				window.open(url, '_blank');
				window.focus();
			}
			else if(cvaction == "save")
			{
				if(result != "NOTLOGGEDIN")
				{
					cv = result.split("]");
					jQuery('#loadcvid').val(result);
					jQuery('#loadpanel').html('');
					jQuery('#loadpanel').html('Working on ' + cv[0] + "]");
					jQuery("#cvnameloaded").html( cv[0] + "]");
					alert("CV '" + cv[0] + "]' has been successfully saved!");
					//location.reload(true);
				}
				else
					alert("In order to save your cv in our system you have to log in first!\nAlternatively you can download the produced JSON and load it later for editing!");
			}
			else if(cvaction == "preview")
			{
				if(cvlayout == "default")
					window.open("<?php echo plugin_dir_url( __FILE__ );  ?>layouts/default.php", "_blank", "toolbar=no, scrollbars=yes, menubar=yes, resizable=no, width=813, height=1100");
				else if(cvlayout == "simpleblack")
					window.open("<?php echo plugin_dir_url( __FILE__ );  ?>layouts/simpleblack.php", "_blank", "toolbar=no, scrollbars=yes, menubar=yes, resizable=no, width=813, height=1100");
				else if(cvlayout == "colorful")
					window.open("<?php echo plugin_dir_url( __FILE__ );  ?>layouts/colorful.php", "_blank", "toolbar=no, scrollbars=yes, menubar=yes, resizable=no, width=813, height=1100");
				else if(cvlayout == "splitblue")
					window.open("<?php echo plugin_dir_url( __FILE__ );  ?>layouts/splitblue.php", "_blank", "toolbar=no, scrollbars=yes, menubar=yes, resizable=no, width=813, height=1100");
				else if(cvlayout == "splitblack")
					window.open("<?php echo plugin_dir_url( __FILE__ );  ?>layouts/splitblack.php", "_blank", "toolbar=no, scrollbars=yes, menubar=yes, resizable=no, width=813, height=1100");
				else if(cvlayout == "splitcolorful")
					window.open("<?php echo plugin_dir_url( __FILE__ );  ?>layouts/splitcolorful.php", "_blank", "toolbar=no, scrollbars=yes, menubar=yes, resizable=no, width=813, height=1100");
				else
					alert("This layout is not available yet!");
			}
			else if(cvaction == "download" && cvtype == 'pdf')
			{
				if(cvlayout == "default")
					window.open("<?php echo plugin_dir_url( __FILE__ );  ?>layouts/createdefault.php", "_blank", "toolbar=yes, scrollbars=yes, resizable=no, width=900, height=900");
				else if(cvlayout == "simpleblack")
					window.open("<?php echo plugin_dir_url( __FILE__ );  ?>layouts/createsimpleblack.php", "_blank", "toolbar=yes, scrollbars=yes, resizable=no, width=900, height=900");
				else if(cvlayout == "colorful")
					window.open("<?php echo plugin_dir_url( __FILE__ );  ?>layouts/createcolorful.php", "_blank", "toolbar=yes, scrollbars=yes, resizable=no, width=900, height=900");
				else if(cvlayout == "splitblack")
					window.open("<?php echo plugin_dir_url( __FILE__ );  ?>layouts/createsplitblack.php", "_blank", "toolbar=yes, scrollbars=yes, resizable=no, width=900, height=900");
				else if(cvlayout == "splitblue")
					window.open("<?php echo plugin_dir_url( __FILE__ );  ?>layouts/createsplitblue.php", "_blank", "toolbar=yes, scrollbars=yes, resizable=no, width=900, height=900");
				else if(cvlayout == "splitcolorful")
					window.open("<?php echo plugin_dir_url( __FILE__ );  ?>layouts/createsplitcolorful.php", "_blank", "toolbar=yes, scrollbars=yes, resizable=no, width=900, height=900");
				else
					alert("This layout is not available yet!");
			}
			else
				alert('Not available yet!\nBe patient and send me some love!');
		},
		error: function(result){
			alert('Uknown Error!');
		}
	});
}

</script>

<div style="width: 100%; padding:0; margin: 0;">  
<button id="btn-cv-save" class="taboptions cvtabbtnpressed">1. Load / Choose CV</button>
<button id="btn-cv-content" class="taboptions cvtabbtn">2. Insert / Edit Content</button>
<button id="btn-cv-layout" class="taboptions cvtabbtn">3. Choose Layout & Save</button>
</div>

<div id="cv-save" class="withcurves">
  <div style="padding:5px; font-size:12px;">
  <?php
  if ( is_user_logged_in() ) 
  {
    echo '<div style="font-size: 15px; font-weight: bold; text-align: center;">Hello, '.$current_user->user_login."</div>";
    
    echo '<div id="loadpanel" style="font-weight: bold; font-size: 12px; text-align:center;"></div><hr class="cvhr">';
    $cvdirectory = "wp-content/plugins/estros-cvmaker/cvs";
    //echo "CV DIR: ".$cvdirectory."<br>";
    $cvfiles = getFileList($cvdirectory, $current_user->user_login, $current_user->ID);
    ?>
    <?php
    if(sizeof($cvfiles) > 0)
    {
      ?>
      <div style="font-size: 12px; display:inline-block; width: 100%;">
        <strong>Saved CVs</strong><br />
        <select id="mycvs" name="mycvs" class="cvmakertextfield" style="display:inline-block; max-width: 250px;">
        <?php
        for($i = 0; $i < sizeof($cvfiles); $i++)
        {
          ?>
          <option value="<?php echo $cvfiles[$i]["name"]; ?>"><?php echo $cvfiles[$i]["shortname"]; ?></option>
          <?php
        }
        ?>
        </select>
        <button class="cvmaker-button" id="loadcv" style="display:inline-block;">Load CV</button>
        <button class="cvmaker-button" id="deletecv" style="display:inline-block;">Delete CV</button>
        <button class="cvmaker-button" id="newcv">New CV</button>
      </div>
      <?php
    }
		else
			echo '<div style="text-align: center;"><button class="cvmaker-button" id="newcv">New CV</button></div>';
  }
  else
  {
    echo "In order to save your cv in our system you have to signin!<br>Alternatively you can save the produced JSON and load it later for editing!<br>";
		echo '<button class="cvmaker-button" id="newcv">New CV</button>';
  }
  ?>
             
  <hr class="cvhr" />
  <div style="font-size: 12px;">
    <strong>Load your CV in json format</strong><br />
    <input type="file" name="filecv" id="filecv" style="font-size:12px;  border: 3px solid #efefef; display:inline-block;">
    <button class="cvmaker-button" name="loadfilecv" id="loadfilecv">Load CV from File</button>
    </div>
  </div>
  <input type="hidden" value="" id="editid" />
  <input type="hidden" value="" id="loadcvid" />
</div>
<div id="cv-content" class="withcurves" >
  <div class="cvsidebar">
    <button class="cvmaker-button" id="contactinfobtn" style="font-size:10px;">Contact</button>
    <button class="cvmaker-button" id="next1">Education</button>
    <button class="cvmaker-button" id="next2">Working<br />Experience</button>
    <button class="cvmaker-button" id="next3">Publications</button>
    <button class="cvmaker-button" id="next4">Working<br />Skills</button>
  
    <button class="cvmaker-button" id="cvmakerbtn-certs">Certifications</button>
    <button class="cvmaker-button" id="cvmakerbtn-awards">Awards</button>
    <button class="cvmaker-button" id="cvmakerbtn-qualifications">Qualifications</button>
  
    <button class="cvmaker-button" id="next5" >Languages</button>
    <button class="cvmaker-button" id="next6" >Hobbies<br />Activities</button>
  </div>
<div class="cvmakerloader">
  <div id="cvmaker-contactinfo" style="width:100%;">
    <div class="headTitle"> Contact Information </div>
    <div>
      <div class="cvinlineblock">
        <div><strong>Fullname</strong></div>
        <div> <input type="text" name="fullname" id="fullname" class="cvmakertextfield" ></div>
      </div>
      <div class="cvinlineblock">
        <div><strong>Email</strong></div>
        <div> <input type="text" name="emailaddresses" id="emailaddresses" class="cvmakertextfield" ></div>
        <div class="cvsupersmall">If you have more than one email, separate with commas</div>
      </div>
    </div>
    <div>
      <div class="cvinlineblock">
        <div><strong>Job Title</strong></div>
        <div> <input type="text" name="jobtitle" id="jobtitle" class="cvmakertextfield" ></div>
      </div>
      <div class="cvinlineblock">
        <div><strong>Birth Date</strong></div>
        <div> <input type="text" name="birthdate" id="birthdate" class="cvmakertextfield" ></div>
      </div>
    </div>
    <div>
      <div class="cvinlineblock">
        <div><strong>Phone Numbers</strong></div>
        <div> <input type="text" name="phonenumbers" id="phonenumbers" class="cvmakertextfield" ></div>
        <div class="cvsupersmall">If you have more than one number, separate with commas</div>
      </div>
      <div class="cvinlineblock">
        <div><strong>Websites</strong></div>
        <div> <input type="text" name="websites" id="websites" class="cvmakertextfield"></div>
        <div class="cvsupersmall">If you have more than one website, separate with commas</div>
      </div>
    </div>
    <hr class="cvhr"/>
    <div>
      <div class="cvinlineblock">
        <div><strong>Address Line 1</strong></div>
        <div> <input type="text" name="adline1" id="adline1" class="cvmakertextfield"></div>
      </div>
      <div class="cvinlineblock">
        <div><strong>Address Line 2</strong></div>
        <div> <input type="text" name="adline2" id="adline2" class="cvmakertextfield"></div>
      </div>
    </div>
    <div>
      <div class="cvinlineblock">
        <div><strong>Address Line 3</strong></div>
        <div> <input type="text" name="adline3" id="adline3" class="cvmakertextfield"></div>
      </div>
      <div class="cvinlineblock">
        <div><strong>Nationality</strong></div>
        <div> <input type="text" name="nationality" id="nationality" class="cvmakertextfield"></div>
      </div>
    </div>
   
    <div style="font-size: 12px;">
    <strong>Short Description (200 chars max)</strong><br />
      <textarea id="contactdescription" class="cvtextarea"></textarea>
    </div>
  </div>
  <div id="cvmaker-education"  style="width:100%;">
    <div class="headTitle"> Education </div>
    <div id="insertEducationList"></div>
    <div>
      <div class="cvinlineblock">
      	<div><strong>Institution Name</strong></div>
        <div> <input type="text" name="institution" id="institution" class="cvmakertextfield"></div>
      </div>
      <div class="cvinlineblock">
      	<div><strong>Degree Type</strong></div>
        <div> <input type="text" name="degreetype" id="degreetype" class="cvmakertextfield"></div>
      </div>
    </div>
    <div>
      <div class="cvinlineblock">
      	<div><strong>Degree Title</strong></div>
        <div> <input type="text" name="degreetitle" id="degreetitle" class="cvmakertextfield"></div>
      </div>
      <div class="cvinlineblock">
      	<div><strong>Grade</strong></div>
        <div> <input type="text" name="degreegrade" id="degreegrade" class="cvmakertextfield"></div>
      </div>
    </div>
    <div>
      <div class="cvinlineblock">
      	<div><strong>From</strong></div>
        <div> <input type="text" name="institutionfrom" id="institutionfrom" class="cvmakertextfield"></div>
      </div>
      <div class="cvinlineblock">
      	<div><strong>To</strong></div>
        <div> <input type="text" name="institutionto" id="institutionto" class="cvmakertextfield"></div>
      </div>
    </div>
    <div style="font-size: 12px;">
    	<strong>Description</strong><br />
      <textarea rows="20" id="institutiondescription" class="cvtextarea"></textarea>
    </div>
    <div>
      <div class="cvinlineblock">
      </div>
      <div class="cvinlineblock" style="text-align:right; ">
      	<button class="cvmaker-button canceledit">New Entry</button><button class="cvmaker-button" id="addeducation"> Save Entry</button>
      </div>
    </div>
  </div>
  <div id="cvmaker-experience">
    <div class="headTitle"> Working Experience </div>
    <div id="insertWorkExList"></div>
    <div>
      <div class="cvinlineblock">
        <div><strong>Company Name</strong></div>
        <div> <input type="text" name="wxcompanyname" id="wxcompanyname" class="cvmakertextfield"></div>
      </div>
      <div class="cvinlineblock">
        <div><strong>Job Position</strong></div>
        <div> <input type="text" name="wxjobposition" id="wxjobposition" class="cvmakertextfield"></div>
      </div>
    </div>
    <div>
      <div class="cvinlineblock">
        <div><strong>From</strong></div>
        <div> <input type="text" name="wxfrom" id="wxfrom" class="cvmakertextfield"></div>
      </div>
      <div class="cvinlineblock">
        <div><strong>To</strong></div>
        <div> <input type="text" name="wxto" id="wxto" class="cvmakertextfield"></div>
      </div>
    </div>
    <div style="font-size: 12px;">
      <strong>Description</strong><br />
      <textarea rows="20" id="wxdescription" class="cvtextarea"></textarea>
    </div>
    <div>
      <div class="cvinlineblock">
      </div>
      <div class="cvinlineblock" style="text-align:right; ">
        <button class="cvmaker-button canceledit">New Entry</button><button class="cvmaker-button" id="addwx"> Save Entry</button>
      </div>
    </div>
  </div>
  
  <div id="cvmaker-publications">
    <div class="headTitle"> Publications </div>
    <div id="insertPublicationsList"></div>
    <div>
      <div class="cvinlineblock">
        <div><strong>Title</strong></div>
        <div> <input type="text" name="publicationtitle" id="publicationtitle" class="cvmakertextfield"></div>
      </div>
      <div class="cvinlineblock">
        <div><strong>Publication Date</strong></div>
        <div> <input type="text" name="publicationdate" id="publicationdate" class="cvmakertextfield"></div>
      </div>
    </div>
    <div style="font-size: 12px;">
    	<strong>Link</strong><br />
      <input type="text" name="publicationlink" id="publicationlink" class="cvsimpleline">
    </div>
    <div style="font-size: 12px;">
      <strong>Description</strong><br />
      <textarea rows="20" id="publicationdescription" name="publicationdescription" class="cvtextarea"></textarea>
    </div>
    <div>
      <div class="cvinlineblock">
      </div>
      <div class="cvinlineblock" style="text-align:right; ">
        <button class="cvmaker-button canceledit">New Entry</button><button class="cvmaker-button" id="addpublications"> Save Entry</button>
      </div>
    </div>
  </div>
  
  <div id="cvmaker-certifications">
    <div class="headTitle"> Certifications </div>
    <div id="insertCertificationsList"></div>
    <div>
      <div class="cvinlineblock">
        <div><strong>Title</strong></div>
        <div> <input type="text" name="certificationtitle" id="certificationtitle" class="cvmakertextfield"></div>
      </div>
      <div class="cvinlineblock">
        <div><strong>Date</strong></div>
        <div> <input type="text" name="certificationdate" id="certificationdate" class="cvmakertextfield"></div>
      </div>
    </div>
    <div style="font-size: 12px;">
    	<strong>Institution</strong><br />
      <input type="text" name="certificationinstitution" id="certificationinstitution" class="cvsimpleline">
    </div>
    <div style="font-size: 12px;">
      <strong>Description</strong><br />
      <textarea rows="20" id="certificationdescription" name="certificationdescription" class="cvtextarea"></textarea>
    </div>
    <div>
      <div class="cvinlineblock">
      </div>
      <div class="cvinlineblock" style="text-align:right; ">
        <button class="cvmaker-button canceledit">New Entry</button><button class="cvmaker-button" id="addcertification"> Save Entry</button>
      </div>
    </div>
  </div>
  
  <div id="cvmaker-awards">
    <div class="headTitle"> Awards </div>
    <div id="insertAwardsList"></div>
    <div>
      <div class="cvinlineblock">
        <div><strong>Title</strong></div>
        <div> <input type="text" name="awardtitle" id="awardtitle" class="cvmakertextfield"></div>
      </div>
      <div class="cvinlineblock">
        <div><strong>Date</strong></div>
        <div> <input type="text" name="awarddate" id="awarddate" class="cvmakertextfield"></div>
      </div>
    </div>
    <div style="font-size: 12px;">
    	<strong>Institution</strong><br />
      <input type="text" name="awardinstitution" id="awardinstitution" class="cvsimpleline">
    </div>
    <div style="font-size: 12px;">
      <strong>Description</strong><br />
      <textarea rows="20" id="awarddescription" name="awarddescription" class="cvtextarea"></textarea>
    </div>
    <div>
      <div class="cvinlineblock">
      </div>
      <div class="cvinlineblock" style="text-align:right; ">
        <button class="cvmaker-button canceledit">New Entry</button><button class="cvmaker-button" id="addaward"> Save Entry</button>
      </div>
    </div>
  </div>
  
  <div id="cvmaker-qualifications">
    <div class="headTitle"> Qualifications </div>
    <div id="insertQualificationsList"></div>
    <div>
      <div class="cvinlineblock">
        <div><strong>Title</strong></div>
        <div> <input type="text" name="qualificationtitle" id="qualificationtitle" class="cvmakertextfield"></div>
      </div>
      <div class="cvinlineblock">
        <div><strong>Date</strong></div>
        <div> <input type="text" name="qualificationdate" id="qualificationdate" class="cvmakertextfield"></div>
      </div>
    </div>
    <div style="font-size: 12px;">
    	<strong>Institution</strong><br />
      <input type="text" name="qualificationinstitution" id="qualificationinstitution" class="cvsimpleline">
    </div>
    <div style="font-size: 12px;">
      <strong>Description</strong><br />
      <textarea rows="20" id="qualificationdescription" name="qualificationdescription" class="cvtextarea"></textarea>
    </div>
    <div>
      <div class="cvinlineblock">
      </div>
      <div class="cvinlineblock" style="text-align:right; ">
        <button class="cvmaker-button canceledit">New Entry</button><button class="cvmaker-button" id="addqualification"> Save Entry</button>
      </div>
    </div>
  </div>
  
  <div id="cvmaker-wskills">
    <div class="headTitle"> Working Skills </div>
    <div id="insertWskillsList"></div>
    <div>
      <div class="cvinlineblock">
        <div><strong>Skill Type</strong></div>
        <div> <input type="text" name="wstype" id="wstype" class="cvmakertextfield"></div>
      </div>
      <div class="cvinlineblock">
        <div><strong>Skill Name</strong></div>
        <div> <input type="text" name="skillname" id="skillname" class="cvmakertextfield"></div>
      </div>
    </div>
    <div>
      <div class="cvinlineblock">
      </div>
      <div class="cvinlineblock" style="text-align:right; padding-top:10px;">
        <button class="cvmaker-button canceledit">New Entry</button><button class="cvmaker-button" id="addskill"> Save Entry</button>
      </div>
    </div>
  </div>
  <div id="cvmaker-languages">
    <div class="headTitle"> Languages </div>
    <div id="insertLanguagesList"></div>
    <div>
      <div class="cvinlineblock">
        <div><strong>Language</strong></div>
        <div> <input type="text" name="languagename" id="languagename" class="cvmakertextfield"></div>
      </div>
      <div class="cvinlineblock">
        <div><strong>Level</strong></div>
        <div> <input type="text" name="languagelevel" id="languagelevel" class="cvmakertextfield"></div>
      </div>
    </div>
    <div>
      <div class="cvinlineblock">
        <div><strong>Certificate</strong></div>
        <div> <input type="text" name="langcertname" id="langcertname" class="cvmakertextfield"></div>
      </div>
      <div class="cvinlineblock">
        <div><strong>Grade</strong></div>
        <div> <input type="text" name="langcertgrade" id="langcertgrade" class="cvmakertextfield"></div>
      </div>
    </div>
    <div>
      <div class="cvinlineblock">
      </div>
      <div class="cvinlineblock" style="text-align:right; padding-top:10px;">
        <button class="cvmaker-button canceledit">New Entry</button><button class="cvmaker-button" id="addlanguage"> Save Entry</button>
      </div>
    </div>
  </div>
  
  <div id="cvmaker-activities">
    <div class="headTitle"> Hobbies & Activities </div>
    <div style="padding: 0px 0px 10px 5px; font-size: 12px;">
    <strong>Hobbies & Activities (500 chars max)</strong><br />
      <textarea rows="20" id="hobbies" class="cvtextarea"></textarea>
    </div>
  </div>
  
  

  <input id="token" name="token" type="hidden" value="u7y3cohysyiqDT3y9t1hVp32szhIsXlXdW7HTFh1" />
  
</div>
</div>
<div id="cv-layout" class="withcurves">
<div id="cvmaker-layout">
    <div class="headTitle"> Choose Layout </div>
    <div style="font-size:12px;">
      <div style="width: 100%; text-align:center;">
      	<div><strong>Name your CV for future use</strong></div>
        <div id="cvnameloaded"></div>
        <div><input type="text" name="cvsavename" id="cvsavename" class="cvmakertextfield" style="width:300px !important;"></div>
        <div class="cvsupersmall">Only letters, numbers and underscore ('_') are allowed!</div><br />
      </div> 
      <div>
       <div style="font-size: 14px; text-align:center;"><strong>Layouts</strong></div>
       <table style="margin:0; padding:0; border:0; text-align:center; width: 100%;">
       	<tr>
        	<td class="cv-table">
          	<input type="checkbox" name="cvlayouts[]" value="default" checked/> Default<br />
            <img src="<?php echo plugin_dir_url( __FILE__ );  ?>images/default.jpg" />
          </td>
          <td class="cv-table">
          	<input type="checkbox" name="cvlayouts[]" value="simpleblack"/> Simple Black<br />
            <img src="<?php echo plugin_dir_url( __FILE__ );  ?>images/simpleblack.jpg" />
          </td>
          <td class="cv-table">
          	<input type="checkbox" name="cvlayouts[]" value="colorful"/> Colorful<br />
            <img src="<?php echo plugin_dir_url( __FILE__ );  ?>images/colorful.jpg" />
          </td>
        </tr>
        <tr>
        	<td class="cv-table">
          	<input type="checkbox" name="cvlayouts[]" value="splitblack"/> Split Black<br />
          	<img src="<?php echo plugin_dir_url( __FILE__ );  ?>images/splitblack.jpg" />
          </td>
          <td class="cv-table">
          	<input type="checkbox" name="cvlayouts[]" value="splitblue"/> Split Blue<br />
          	<img src="<?php echo plugin_dir_url( __FILE__ );  ?>images/splitblue.jpg" />
          </td>
          <td class="cv-table">
          	<input type="checkbox" name="cvlayouts[]" value="splitcolorful"/> Split Colorful<br />
          	<img src="<?php echo plugin_dir_url( __FILE__ );  ?>images/splitcolorful.jpg" />
          </td>
        </tr>
       </table>
        
        
        
        
      </div>
    </div>
    <div style="width: 100%; text-align: center; margin-top:15px; padding-top:3px;border-top: 3px solid #efefef;">
    <button onClick="actionsCV('save', '');" class="cvmaker-button">Save CV</button>
    <button onClick="actionsCV('preview', '');" class="cvmaker-button">Preview/Download HTML</button>
    <button onClick="actionsCV('download', 'json');" class="cvmaker-button"/>Download Data File</button>
    <button onClick="actionsCV('download', 'pdf');" class="cvmaker-button"/>Download PDF</button>
  </div>
  </div>
</div>