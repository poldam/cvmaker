<?php

function cleanInput($data) {
	$data = str_replace(">","›",$data);
	$data = str_replace("<","‹",$data);
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

function generateRandomString($length = 8) {
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return $randomString;
}

if(isset($_POST['token']) && $_POST['token'] == "u7y3cohysyiqDT3y9t1hVp32szhIsXlXdW7HTFh1")
{
	//require_once('../../../wp-config.php');
	
	$cvaction = $_POST['cvaction'];
	$cvtype = $_POST['cvtype'];
	
	$loadcvid = $_POST['loadcvid'];

	$fullname = isset($_POST['fullname']) ? $_POST['fullname'] : "";
	$emailaddress = explode(",", isset($_POST['emailaddress']) ? cleanInput($_POST['emailaddress']) : "");
	$websites = explode(",", isset($_POST['websites']) ? cleanInput($_POST['websites']) : "");
	$phonenumbers = explode(",", isset($_POST['phonenumbers']) ? cleanInput($_POST['phonenumbers']) : "");
	$adline1 = isset($_POST['adline1']) ? $_POST['adline1'] : "";
	$adline2 = isset($_POST['adline2']) ? $_POST['adline2'] : "";
	$adline3 = isset($_POST['adline3']) ? $_POST['adline3'] : "";
	$jobtitle = isset($_POST['jobtitle']) ? $_POST['jobtitle'] : "";
	$birthdate = isset($_POST['birthdate']) ? $_POST['birthdate'] : "";
	$nationality = isset($_POST['nationality']) ? $_POST['nationality'] : "";
	$contactdescription = isset($_POST['contactdescription']) ? $_POST['contactdescription'] : "";
	$hobbies = isset($_POST['hobbies']) ? $_POST['hobbies'] : "";
	$workex = array();
	$education = array();
	$publications = array();
	
	$qualifications = array();
	$awards = array();
	$certifications = array();
	
	$skills = array();
	$languages = array();
	$cvlayout = "default";
	$username = $_POST['username'];
	$userid = $_POST['userid'];
	$cvsavename = !empty($_POST['cvsavename']) ? cleanInput($_POST['cvsavename'])."[".date("Y-m-d")."]" : "MyCV[".date("Y-m-d")."]";
	
	if(isset($_POST['cvlayout']) && $_POST['cvlayout'] != "")
		$cvlayout = $_POST['cvlayout'];
	
	if(isset($_POST['education']))
		for($i = 0; $i < sizeof($_POST['education']); $i++)
			$education[$i] = explode("~", cleanInput($_POST['education'][$i])); 

	if(isset($_POST['workex']))
		for($i = 0; $i < sizeof($_POST['workex']); $i++)
			$workex[$i] = explode("~", cleanInput($_POST['workex'][$i])); 
	
	if(isset($_POST['publications']))
		for($i = 0; $i < sizeof($_POST['publications']); $i++)
			$publications[$i] = explode("~", cleanInput($_POST['publications'][$i]));
			
	if(isset($_POST['awards']))
		for($i = 0; $i < sizeof($_POST['awards']); $i++)
			$awards[$i] = explode("~", cleanInput($_POST['awards'][$i]));
			
	if(isset($_POST['certifications']))
		for($i = 0; $i < sizeof($_POST['certifications']); $i++)
			$certifications[$i] = explode("~", cleanInput($_POST['certifications'][$i]));
			
	if(isset($_POST['qualifications']))
		for($i = 0; $i < sizeof($_POST['qualifications']); $i++)
			$qualifications[$i] = explode("~", cleanInput($_POST['qualifications'][$i]));
	
	if(isset($_POST['skills']))
		for($i = 0; $i < sizeof($_POST['skills']); $i++)
			$skills[$i] = explode("~", cleanInput($_POST['skills'][$i]));

	if(isset($_POST['languages']))
		for($i = 0; $i < sizeof($_POST['languages']); $i++)
			$languages[$i] = explode("~", cleanInput($_POST['languages'][$i]));
	
	
	$cvjson = array(
		"fullname" => cleanInput($fullname),
		"emailaddress" => $emailaddress,
		"phonenumbers" => $phonenumbers,
		"websites" => $websites,
		"adline1" => cleanInput($adline1),
		"adline2" => cleanInput($adline2),
		"adline3" => cleanInput($adline3),
		"jobtitle" => cleanInput($jobtitle),
		"birthdate" => cleanInput($birthdate),
		"nationality" => cleanInput($nationality),
		"contactdescription" => cleanInput($contactdescription),
		"hobbies" => cleanInput($hobbies),
		"education" => $education,
		"workex" => $workex,
		"awards" => $awards,
		"qualifications" => $qualifications,
		"certifications" => $certifications,
		"publications" => $publications,
		"skills" => $skills,
		"languages" => $languages,
		"cvlayout" => $cvlayout
	);

	if($cvaction == "download" && $cvtype == "json")
		echo json_encode($cvjson);
	else if($cvaction == "save")
	{
		if($username != "NONE" && $userid != "NONE")
		{
			//echo $loadcvid;
			if($loadcvid == "")
				$file = $cvsavename."-".$userid."-".$username."-".generateRandomString();
			else
				$file = $loadcvid;
			echo $file;

			file_put_contents("cvs/".$file, json_encode($cvjson, TRUE));
		}
		else
		{
			echo "NOTLOGGEDIN";
		}
	}
	else if($cvaction == "preview")
	{
		session_start();
		$_SESSION['cv'] = $cvjson;
	}
	else if($cvaction == "download")
	{
		session_start();
		$_SESSION['cv'] = $cvjson;
	}
	else
		echo "NOT IMPLEMENTED YET. BE PATIENT AND BUY ME SOME CHOCOLATE!";
	
}
else
{
	echo "Indirect access of the form is not allowed!";
}
?>