<?php
	session_start();
	$cv = $_SESSION['cv'];
	$education = $cv['education'];
	$workex = $cv['workex'];
	$skills = $cv['skills'];
	$languages = $cv['languages'];
	$publications = $cv['publications'];
	
	$certifications = $cv['certifications'];
	$awards = $cv['awards'];
	$qualifications = $cv['qualifications'];

	echo '<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>CV - '.$cv['fullname'].'</title>
    <style>
		body
		{
			font-size:11px;
			font-weight:normal;
			font-family: Helvetica;
			color:#444444;
			margin: 0 auto; 
			width: 794px;
		}
		table, tr, td
		{
			padding:0;
			margin:0;
			vertical-align:top;
			font-size:11px;
		}
		
		ul, li
		{
			margin: 0;
			padding: 0;
		}
		ul
		{
			padding-left:15px;
		}
		li
		{
			padding-bottom: 10px;
		}
		
		h3
		{
			text-transform: uppercase; 
			font-size: 14px; 
			color: #333333; 
			border-bottom: 1px solid #333333; 
			border-top: 1px solid #333333; 
			padding-top: 5px; 
			padding-bottom: 5px;
		}
		
    </style>
  </head>

  <body>
    <div style="text-align: left; background-color: #333333; height: 100px; padding-top:40px; padding-left:30px; width: 100%;">
      	<div style="width: 365px; display:inline-block;vertical-align:top;">';
				if($cv['fullname'] != "")
        {  
					echo '<span style="font-size:28px; color: white;">'.$cv['fullname'].'</span><br>';
				} 
        if($cv['jobtitle'] != "")
        {  
        	echo '<span style="font-size:12px; color: white;">'.$cv['jobtitle'].'</span>';
				} 
        if($cv['nationality'] != "" || $cv['birthdate'] != "")
        {  
        	echo '<span style="font-size:10px; color: white;">';
					if($cv['nationality'] != "")
						echo ", ".$cv['nationality']; 
					if($cv['birthdate'] != "")
						echo ", ".$cv['birthdate'];
        	echo '</span>'; 
				} 
       echo '</div>
       <div style="display:inline-block;vertical-align:top; color: #cccccc;">';
				for($i = 0; $i < sizeof($cv['emailaddress']); $i++)
					echo trim($cv['emailaddress'][$i])."<br>";
        for($i = 0; $i < sizeof($cv['phonenumbers']); $i++)
					echo trim($cv['phonenumbers'][$i])."<br>";
				for($i = 0; $i < sizeof($cv['websites']); $i++)
					echo trim($cv['websites'][$i])."<br>";
			echo '
      </div>
      <div style="display:inline-block;vertical-align:top; color: #cccccc; padding-left:40px;">
      ';
				if($cv['adline1'] != "")
					echo $cv['adline1']; 
				if($cv['adline2'] != "")
					echo ", ".$cv['adline2'];
				if($cv['adline3'] != "")
					echo "<br>".$cv['adline3']."<br>"; 
			echo '
      </div>
      </div>
<div style="width: 100%;">
      <div style="width:329px; padding:30px; height: 100%; background-color: #fff; padding-top: 30px; float: left;">';
      
      
        if($cv['contactdescription'] != "")
        { 
          echo '<h3>Profile</h3>';
          echo '<div style="text-align:justify;">'.$cv['contactdescription'].'</div><br>';
				} 
        
				if(sizeof($workex) > 0)
        { 
        	echo '<h3>Working Experience</h3>';
      
					for($i = 0; $i < sizeof($workex); $i++)	
					{
						echo "<div><ul><li>";
						if($workex[$i][0] != "")
							echo "<strong>".$workex[$i][0]."</strong>";
						if($workex[$i][1] != "")
							echo " <strong>".$workex[$i][1]."</strong>";
						if($workex[$i][2] != "")
							echo " / <span style='color:#aaa;'>".$workex[$i][2]."</span>";
						if($workex[$i][3] != "")
							echo "<span style='color:#aaa;'> - ".$workex[$i][3]."</span>";
						echo "</li></ul></div>
						<div style='padding-bottom: 10px; text-align: justify;'>";
						
						if($workex[$i][4] != "")
							echo $workex[$i][4];
						echo "</div>";
					}
					
				} 
				
				if(sizeof($certifications) > 0)
        { 
        	echo '<h3>Certifications';
        if(sizeof($awards) > 0)
        { 
        	echo ' & Awards</h3>';
        
				} else {echo "</h3>";} 
 
						for($i = 0; $i < sizeof($certifications); $i++)	
						{
							echo "<div><ul><li>";
							if($certifications[$i][0] != "")
								echo "<strong>".$certifications[$i][0]."</strong>";
							if($certifications[$i][1] != "")
								echo " / <span style='color: #aaaaaa;'>".$certifications[$i][1]."</span>";
							echo "</li></ul>";
							if($certifications[$i][2] != "")
								echo $certifications[$i][2];
							if($certifications[$i][3] != "")
								echo "<div style='padding-top: 10px; text-align: justify;'>".$certifications[$i][3]."</div><br>";
							echo "</div>";
						}
				} 
				
				if(sizeof($awards) > 0 && sizeof($certifications) <= 0)
        { 
        	echo '<h3>Awards</h3>';
         
				} 
				for($i = 0; $i < sizeof($awards); $i++)	
				{
					echo "<div>";
					echo "<ul><li>";
					if($awards[$i][0] != "")
						echo "<strong>".$awards[$i][0]."</strong>";
					if($awards[$i][1] != "")
						echo " / <span style='color: #aaaaaa;'>".$awards[$i][1]."</span>";
					echo "</li></ul>";
					if($awards[$i][2] != "")
						echo "<strong>".$awards[$i][2]."</strong>";
					if($awards[$i][3] != "")
						echo " / ".$awards[$i][3];
					echo "<br><br>";
					echo "</div>";
				}
					
			 if(sizeof($publications) > 0)
        { 
        	echo '<h3>Publications</h3>';
					for($i = 0; $i < sizeof($publications); $i++)	
					{
						echo "<div><ul><li>";
						if($publications[$i][0] != "")
							echo "<strong>".$publications[$i][0]."</strong>";
						if($publications[$i][1] != "")
							echo " / <span style='color: #aaaaaa;'>".$publications[$i][1]."</span>";
						echo "</li></ul></div><div>";
						if($publications[$i][2] != "")
							echo "<div style='padding-bottom: 10px;'><strong>Link:</strong> ".$publications[$i][2]."</div>";
						if($publications[$i][3] != "")
							echo "<div style='padding-bottom: 10px; text-align: justify;'>".$publications[$i][3]."</div>";
						echo "</div>";
					}
				} 
      
       echo '<br>
      </div>
      <div style="width:339px; background-color:#F2F2F2; padding:30px; padding-top:30px; float: right;">';

      if(sizeof($education) > 0)
			{ 
			 	echo '<h3>Education';
			 	if(sizeof($qualifications) > 0)
				{ 
					echo ' & Qualifications</h3>';
      	 } else { echo "</h3>"; }
				}
			 if(sizeof($education) > 0)
        { 

					for($i = 0; $i < sizeof($education); $i++)	
					{
						echo "<div><ul><li>";
						if($education[$i][2] != "")
							echo "<strong>".$education[$i][2]."<br></strong>";
						if($education[$i][0] != "")
							echo "<strong>".$education[$i][0]."</strong>";
						if($education[$i][4] != "")
							echo "<br><span style='color: #aaaaaa;'>".$education[$i][4]."</span>";
						if($education[$i][5] != "")
							echo "<span style='color: #aaaaaa;'> - ".$education[$i][5]."</span>";
						echo "</li></ul></div>";
						echo "<div>";
						
						if($education[$i][1] != "")
							echo "".$education[$i][1];
						if($education[$i][3] != "")
							echo ", Grade ".$education[$i][3];
						if($education[$i][6] != "")
							echo "<div style='padding-top: 10px; padding-bottom: 10px; text-align: justify;'>".$education[$i][6]."</div>";
						echo "</div>";
					}
				} 
				
				if(sizeof($education) <= 0 && sizeof($qualifications) > 0)
        { 
         	echo '<h3>Qualifications</h3>';
				}
        
        if(sizeof($qualifications) > 0)
        { 
						for($i = 0; $i < sizeof($qualifications); $i++)	
						{
							echo "<div><ul><li>";
							if($qualifications[$i][0] != "")
								echo "<strong>".$qualifications[$i][0]."</strong>";
							if($qualifications[$i][2] != "")
								echo " at ".$qualifications[$i][2];
							if($qualifications[$i][1] != "")
								echo " / <span style='color: #aaaaaa;'>".$qualifications[$i][1]."</span>";
							echo "</li></ul></div><div>";
							if($qualifications[$i][3] != "")
								echo "<div style='padding-bottom: 10px; text-align: justify;'>".$qualifications[$i][3]."</div>";
							echo "</div>";
						}
				} 
				
				if(sizeof($skills) > 0)
        { 
        	echo '<h3>Working Skills';
        	if(sizeof($languages) > 0)
					{ 
						echo ' & Languages
						</h3>';
					} else {echo "</h3>";}
				
        	echo '<div style="width: 180px; display: inline-block; vertical-align: top;">';
						$howmany = sizeof($skills);
						$howmany = $howmany/3;
						echo "<div>";
						$j = 0;
						for($i = 0; $i < sizeof($skills); $i++)	
						{
							if($j == $howmany)
							{
								echo "</div><div>";
								$j = 0;
							}
							echo "<strong>".$skills[$i][0]."</strong>: ".$skills[$i][1]."<br>";
							$j++;
						}
						echo "</div>
          </div>";
					
				} 
        
				if(sizeof($languages) > 0 && sizeof($skills) <= 0)
        { 
         echo '<h3>Languages</h3>';
				}
				echo '<div style="display: inline-block; width: 150px; vertical-align: top;">';
        
				for($i = 0; $i < sizeof($languages); $i++)	
				{
					echo "<div>";
					if($languages[$i][0] != "")
						echo "<strong>".$languages[$i][0]."</strong>";
					if($languages[$i][1] != "")
						echo " - ".$languages[$i][1]." level";
					if($languages[$i][2] != "")
						echo "<br>".$languages[$i][2];
					if($languages[$i][3] != "")
						echo " - ".$languages[$i][3];
					echo "</div><br>";
				}
        echo '</div>';
        
				if($cv['hobbies'] != "")
        { 
					echo '<h3>Hobbies & Activities</h3>
					<div style="text-align: justify;">
						'.$cv['hobbies'].' 
					</div>';
				} 
echo '
      </div>
  </div>
  </body>
 </html> ';
 

	//unset($_SESSION['cv']);
	//session_destroy();
?>