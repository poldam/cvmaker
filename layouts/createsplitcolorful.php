<?php

session_start();
$cv = $_SESSION['cv'];
$education = $cv['education'];
$workex = $cv['workex'];
$skills = $cv['skills'];
$languages = $cv['languages'];
$publications = $cv['publications'];
$certifications = $cv['certifications'];
$awards = $cv['awards'];
$qualifications = $cv['qualifications'];


$html = "";

$html = '<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>CV - '.$cv['fullname'].'</title>
    <style>
		body
		{
			font-size:11px;
			font-weight:normal;
			font-family: Helvetica;
			color:#444444;
			margin: 0 auto; 
			width: 794px;
		}
		table, tr, td
		{
			padding:0;
			margin:0;
			vertical-align:top;
			font-size:11px;
		}
		
		ul, li
		{
			margin: 0;
			padding: 0;
		}
		ul
		{
			padding-left:15px;
		}
		li
		{
			padding-bottom: 10px;
		}
		
		h3
		{
			text-transform: uppercase; 
			font-size: 15px; 
			color: #333333; 
			border-bottom: 1px solid #F49630; 
			border-top: 1px solid #F49630; 
			padding-top: 5px; 
			padding-bottom: 5px;
		}
		
    </style>
  </head>

  <body>
    <div style="text-align: left; background-color: #333333; height: 100px; padding-top:40px; padding-left:30px; width: 100%;">
      	<div style="width: 322px; float: left;vertical-align:top;">';
				if($cv['fullname'] != "")
        {  
					$html .= '<span style="font-size:28px; color: white;">'.$cv['fullname'].'</span><br>';
				} 
        if($cv['jobtitle'] != "")
        {  
        	$html .= '<span style="font-size:12px; color: white;">'.$cv['jobtitle'].'</span>';
				} 
        if($cv['nationality'] != "" || $cv['birthdate'] != "")
        {  
        	$html .= '<span style="font-size:10px; color: white;">';
					if($cv['nationality'] != "")
						$html .= ", ".$cv['nationality']; 
					if($cv['birthdate'] != "")
						$html .= ", ".$cv['birthdate'];
        	$html .= '</span>'; 
				} 
       $html .= '</div>
       <div style="float:right;vertical-align:top; color: #cccccc; width: 183px;">';
			 if($cv['adline1'] != "")
					$html .= $cv['adline1']; 
				if($cv['adline2'] != "")
					$html .= ", ".$cv['adline2'];
				if($cv['adline3'] != "")
					$html .= "<br>".$cv['adline3']."<br>"; 
				
			$html .= '
      </div>
      <div style="float: right;;vertical-align:top; color: #cccccc; width: 180px;">
      ';
				for($i = 0; $i < sizeof($cv['emailaddress']); $i++)
					$html .= trim($cv['emailaddress'][$i])."<br>";
        for($i = 0; $i < sizeof($cv['phonenumbers']); $i++)
					$html .= trim($cv['phonenumbers'][$i])."<br>";
				for($i = 0; $i < sizeof($cv['websites']); $i++)
					$html .= trim($cv['websites'][$i])."<br>";
			$html .= '
      </div>
      </div>
			<div style="width: 100%; margin:0; padding: 0; height: 5px; background-color: #F49630;"></div>
<div style="width: 100%;">
      <div style="width:299px; padding:25px; height: 100%; background-color: #fff; padding-top: 30px; float: left; padding-right: 25px;">';
      
      
        if($cv['contactdescription'] != "")
        { 
          $html .= '<h3>Profile</h3>';
          $html .= '<div style="text-align:justify;">'.$cv['contactdescription'].'</div><br>';
				} 
        
				if(sizeof($workex) > 0)
        { 
        	$html .= '<h3>Working Experience</h3>';
      
					for($i = 0; $i < sizeof($workex); $i++)	
					{
						$html .= "<div><ul><li>";
						if($workex[$i][0] != "")
							$html .= "<strong>".$workex[$i][0]."</strong>";
						if($workex[$i][1] != "")
							$html .= " <strong>".$workex[$i][1]."</strong>";
						if($workex[$i][2] != "")
							$html .= " / <span style='color:#aaa;'>".$workex[$i][2]."</span>";
						if($workex[$i][3] != "")
							$html .= "<span style='color:#aaa;'> - ".$workex[$i][3]."</span>";
						$html .= "</li></ul></div>
						<div style='padding-bottom: 10px; text-align: justify;'>";
						
						if($workex[$i][4] != "")
							$html .= $workex[$i][4];
						$html .= "</div>";
					}
					
				} 
				
				if(sizeof($certifications) > 0)
        { 
        	$html .= '<h3>Certifications';
        if(sizeof($awards) > 0)
        { 
        	$html .= ' & Awards</h3>';
        
				} else {$html .= "</h3>";} 
 
						for($i = 0; $i < sizeof($certifications); $i++)	
						{
							$html .= "<div><ul><li>";
							if($certifications[$i][0] != "")
								$html .= "<strong>".$certifications[$i][0]."</strong>";
							if($certifications[$i][1] != "")
								$html .= " / <span style='color: #aaaaaa;'>".$certifications[$i][1]."</span>";
							$html .= "</li></ul>";
							if($certifications[$i][2] != "")
								$html .= $certifications[$i][2];
							if($certifications[$i][3] != "")
								$html .= "<div style='padding-top: 10px; text-align: justify;'>".$certifications[$i][3]."</div><br>";
							$html .= "</div>";
						}
				} 
				
				if(sizeof($awards) > 0 && sizeof($certifications) <= 0)
        { 
        	$html .= '<h3>Awards</h3>';
         
				} 
				for($i = 0; $i < sizeof($awards); $i++)	
				{
					$html .= "<div>";
					$html .= "<ul><li>";
					if($awards[$i][0] != "")
						$html .= "<strong>".$awards[$i][0]."</strong>";
					if($awards[$i][1] != "")
						$html .= " / <span style='color: #aaaaaa;'>".$awards[$i][1]."</span>";
					$html .= "</li></ul>";
					if($awards[$i][2] != "")
						$html .= "<strong>".$awards[$i][2]."</strong>";
					if($awards[$i][3] != "")
						$html .= " / ".$awards[$i][3];
					$html .= "<br><br>";
					$html .= "</div>";
				}
					
			 if(sizeof($publications) > 0)
        { 
        	$html .= '<h3>Publications</h3>';
					for($i = 0; $i < sizeof($publications); $i++)	
					{
						$html .= "<div><ul><li>";
						if($publications[$i][0] != "")
							$html .= "<strong>".$publications[$i][0]."</strong>";
						if($publications[$i][1] != "")
							$html .= " / <span style='color: #aaaaaa;'>".$publications[$i][1]."</span>";
						$html .= "</li></ul></div><div>";
						if($publications[$i][2] != "")
							$html .= "<div style='padding-bottom: 10px;'><strong>Link:</strong> ".$publications[$i][2]."</div>";
						if($publications[$i][3] != "")
							$html .= "<div style='padding-bottom: 10px; text-align: justify;'>".$publications[$i][3]."</div>";
						$html .= "</div>";
					}
				} 
      
       $html .= '<br>
      </div>
      <div style="width:309px; background-color:#F2F2F2; padding:30px; padding-top:30px; float: right;">';

      if(sizeof($education) > 0)
			{ 
			 	$html .= '<h3>Education';
			 	if(sizeof($qualifications) > 0)
				{ 
					$html .= ' & Qualifications</h3>';
      	 } else { $html .= "</h3>"; }
				}
			 if(sizeof($education) > 0)
        { 

					for($i = 0; $i < sizeof($education); $i++)	
					{
						$html .= "<div><ul><li>";
						if($education[$i][2] != "")
							$html .= "<strong>".$education[$i][2]."<br></strong>";
						if($education[$i][0] != "")
							$html .= "<strong>".$education[$i][0]."</strong>";
						if($education[$i][4] != "")
							$html .= "<br><span style='color: #aaaaaa;'>".$education[$i][4]."</span>";
						if($education[$i][5] != "")
							$html .= "<span style='color: #aaaaaa;'> - ".$education[$i][5]."</span>";
						$html .= "</li></ul></div>";
						$html .= "<div>";
						
						if($education[$i][1] != "")
							$html .= "".$education[$i][1];
						if($education[$i][3] != "")
							$html .= ", Grade ".$education[$i][3];
						if($education[$i][6] != "")
							$html .= "<div style='padding-top: 10px; padding-bottom: 10px; text-align: justify;'>".$education[$i][6]."</div>";
						$html .= "</div>";
					}
				} 
				
				if(sizeof($education) <= 0 && sizeof($qualifications) > 0)
        { 
         	$html .= '<h3>Qualifications';
				}
        
        if(sizeof($qualifications) > 0)
        { 
						for($i = 0; $i < sizeof($qualifications); $i++)	
						{
							$html .= "<div><ul><li>";
							if($qualifications[$i][0] != "")
								$html .= "<strong>".$qualifications[$i][0]."</strong>";
							if($qualifications[$i][2] != "")
								$html .= " at ".$qualifications[$i][2];
							if($qualifications[$i][1] != "")
								$html .= " / <span style='color: #aaaaaa;'>".$qualifications[$i][1]."</span>";
							$html .= "</li></ul></div><div>";
							if($qualifications[$i][3] != "")
								$html .= "<div style='padding-bottom: 10px; text-align: justify;'>".$qualifications[$i][3]."</div>";
							$html .= "</div>";
						}
				} 
				
				if(sizeof($skills) > 0)
        { 
        	$html .= '<h3>Working Skills';
        	if(sizeof($languages) > 0)
					{ 
						$html .= ' & Languages
						</h3>';
					} else {$html .= "</h3>";}
				
        	$html .= '<div style="width: 150px; float: left; vertical-align: top;">';
						$howmany = sizeof($skills);
						$howmany = $howmany/3;
						$html .= "<div>";
						$j = 0;
						for($i = 0; $i < sizeof($skills); $i++)	
						{
							if($j == $howmany)
							{
								$html .= "</div><div>";
								$j = 0;
							}
							$html .= "<strong>".$skills[$i][0]."</strong>: ".$skills[$i][1]."<br>";
							$j++;
						}
						$html .= "</div>
          </div>";
					
				} 
        
				$html .= '<div style=" float: left; width: 150px; vertical-align: top;">';
        if(sizeof($languages) > 0 && sizeof($skills) <= 0)
        { 
         $html .= '<h3>Languages</h3>';
				}
				for($i = 0; $i < sizeof($languages); $i++)	
				{
					$html .= "<div>";
					if($languages[$i][0] != "")
						$html .= "<strong>".$languages[$i][0]."</strong>";
					if($languages[$i][1] != "")
						$html .= " - ".$languages[$i][1]." level";
					if($languages[$i][2] != "")
						$html .= "<br>".$languages[$i][2];
					if($languages[$i][3] != "")
						$html .= " - ".$languages[$i][3];
					$html .= "</div><br>";
				}
        $html .= '</div>';
        
				if($cv['hobbies'] != "")
        { 
					$html .= '<h3>Hobbies & Activities</h3>
					<div style="text-align: justify;">
						'.$cv['hobbies'].' 
					</div>';
				} 
$html .= '
      </div>
  </div>
  </body>
 </html> ';

//echo $html;


//==============================================================
//==============================================================
//==============================================================

include("mpdf.php");

$mpdf=new mPDF('c','A4','','helvetica',10,10,10,10,16,13); 

$mpdf->SetDisplayMode('fullpage');
$mpdf->autoLangToFont = true;
$mpdf->list_indent_first_level = 1;	// 1 or 0 - whether to indent the first level of a list

// LOAD a stylesheet
//$stylesheet = file_get_contents('defaultcss.css');
//$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

$mpdf->WriteHTML($html);

$mpdf->Output();
exit;

?>