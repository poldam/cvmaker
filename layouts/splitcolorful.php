<?php
	session_start();
	$cv = $_SESSION['cv'];
	$education = $cv['education'];
	$workex = $cv['workex'];
	$skills = $cv['skills'];
	$languages = $cv['languages'];
	$publications = $cv['publications'];
	
	$certifications = $cv['certifications'];
	$awards = $cv['awards'];
	$qualifications = $cv['qualifications'];
?>

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>CV - <?php echo $cv['fullname']; ?></title>
    <style>
		body
		{
			font-size:11px;
			font-weight:normal;
			font-family: Helvetica;
			color:#444444;
			margin: 0 auto; 
			width: 794px;
		}
		table, tr, td
		{
			padding:0;
			margin:0;
			vertical-align:top;
			font-size:11px;
		}
		
		ul, li
		{
			margin: 0;
			padding: 0;
		}
		ul
		{
			padding-left:15px;
		}
		li
		{
			padding-bottom: 10px;
		}
		
		h3
		{
			text-transform: uppercase; 
			font-size: 14px; 
			color: #333333; 
			border-bottom: 1px solid #F49630; 
			border-top: 1px solid #F49630; 
			padding-top: 5px; 
			padding-bottom: 5px;
		}
		
    </style>
  </head>

  <body>
  	<table style="width: 100%;">
    <tr style="width: 100%;">
    	<td colspan="2" style="text-align: left; background-color: #333333; height: 130px; padding-top:40px; ">
      	<div style="width: 365px; display:inline-block;vertical-align:top;padding-left:30px; height: 100px;">
				<?php if($cv['fullname'] != "")
        { ?> 
        	<span style="font-size:28px; color: white;"><?php echo $cv['fullname'];  ?></span><br>
        <?php 
				} ?>
        <?php if($cv['jobtitle'] != "")
        { ?> 
        <span style="font-size:12px; color: white;"><?php echo $cv['jobtitle']; ?></span>
        <?php 
				} ?>
        <?php if($cv['nationality'] != "" || $cv['birthdate'] != "")
        { ?> 
        <span style="font-size:10px; color: white;">
					<?php 
						if($cv['nationality'] != "")
							echo ", ".$cv['nationality']; 
						if($cv['birthdate'] != "")
							echo ", ".$cv['birthdate'];
				?>
        </span>
        <?php 
				} ?>
       </div>
       <div style="display:inline-block;vertical-align:top; color: #cccccc; height: 100px;">
       <?php
				for($i = 0; $i < sizeof($cv['emailaddress']); $i++)
					echo trim($cv['emailaddress'][$i])."<br>";
        for($i = 0; $i < sizeof($cv['phonenumbers']); $i++)
					echo trim($cv['phonenumbers'][$i])."<br>";
				for($i = 0; $i < sizeof($cv['websites']); $i++)
					echo trim($cv['websites'][$i])."<br>";
				?>
      </div>
      <div style="display:inline-block;vertical-align:top; color: #cccccc; padding-left:40px; height: 100px;">
      <?php 
				if($cv['adline1'] != "")
					echo $cv['adline1']; 
				if($cv['adline2'] != "")
					echo ", ".$cv['adline2'];
				if($cv['adline3'] != "")
					echo "<br>".$cv['adline3']."<br>"; 
			?>
      </div>
      <div style="width: 100%; margin:0; padding: 0; height: 5px; background-color: #F49630;"></div>
      </td>
    </tr>
    <tr style="width: 100%;">
      <td style="width:329px; padding:30px; height: 100%; background-color: #fff; padding-top: 40px;">
      
      
         <?php if($cv['contactdescription'] != "")
        { ?>
          <h3>Profile</h3>
          <div style="text-align:justify;"><?php echo $cv['contactdescription']; ?></div>
      	<?php 
				} ?>
        
      
      <?php if(sizeof($workex) > 0)
        { ?>
        <h3>Working Experience</h3>
        <table>
        	<?php 
						for($i = 0; $i < sizeof($workex); $i++)	
						{
							echo "<tr>";
							echo "<td><ul><li>";
							if($workex[$i][0] != "")
								echo "<strong>".$workex[$i][0]."</strong>";
							if($workex[$i][1] != "")
								echo " <strong>".$workex[$i][1]."</strong>";
							if($workex[$i][2] != "")
								echo " / <span style='color:#aaa;'>".$workex[$i][2]."</span>";
							if($workex[$i][3] != "")
								echo "<span style='color:#aaa;'> - ".$workex[$i][3]."</span>";
							echo "</li></ul></td></tr>
							<tr>
							<td style='padding-bottom: 10px;'>";
							
							if($workex[$i][4] != "")
								echo $workex[$i][4];
							echo "</td>";
							echo "</tr>";
						}
					?>
        </table>
        <?php 
				} ?>
      
      <?php if(sizeof($certifications) > 0)
        { ?>
        <h3>Certifications 
         <?php if(sizeof($awards) > 0)
        { ?>
        & Awards</h3>
        <?php 
				} else {echo "</h3>";} ?>
        <table>
        	<?php 
						for($i = 0; $i < sizeof($certifications); $i++)	
						{
							echo "<tr>";
							echo "<td><ul><li>";
							if($certifications[$i][0] != "")
								echo "<strong>".$certifications[$i][0]."</strong>";
							if($certifications[$i][1] != "")
								echo " / <span style='color: #aaaaaa;'>".$certifications[$i][1]."</span>";
							echo "</li></ul>";
							if($certifications[$i][2] != "")
								echo $certifications[$i][2];
							if($certifications[$i][3] != "")
								echo "<div style='padding-top: 10px; text-align: justify;'>".$certifications[$i][3]."</div><br>";
							echo "</td>";
							echo "</tr>";
						}
					?>
        </table>
        <?php 
				} ?>
        
        
        
        
        <?php if(sizeof($awards) > 0 && sizeof($certifications) <= 0)
        { ?>
        <h3>Awards</h3>
        <?php 
				} ?>
        <table>
        	<?php 
						for($i = 0; $i < sizeof($awards); $i++)	
						{
							echo "<tr>";
							echo "<td>";
							echo "<ul><li>";
							if($awards[$i][0] != "")
								echo "<strong>".$awards[$i][0]."</strong>";
							if($awards[$i][1] != "")
								echo " / <span style='color: #aaaaaa;'>".$awards[$i][1]."</span>";
							echo "</li></ul>";
							if($awards[$i][2] != "")
								echo "<strong>".$awards[$i][2]."</strong>";
							if($awards[$i][3] != "")
								echo " / ".$awards[$i][3];
							echo "<br><br>";
							echo "</td>";
							echo "</tr>";
						}
					?>
        </table>
      
      <?php if(sizeof($publications) > 0)
        { ?>
        <h3>Publications</h3>
        <table>
        	<?php 
						for($i = 0; $i < sizeof($publications); $i++)	
						{
							echo "<tr>";
							echo "<td><ul><li>";
							if($publications[$i][0] != "")
								echo "<strong>".$publications[$i][0]."</strong>";
							if($publications[$i][1] != "")
								echo " / <span style='color: #aaaaaa;'>".$publications[$i][1]."</span>";
							echo "</li></ul></td></tr><tr><td>";
							if($publications[$i][2] != "")
								echo "<div style='padding-bottom: 10px;'><strong>Link:</strong> ".$publications[$i][2]."</div>";
							if($publications[$i][3] != "")
								echo "<div style='padding-bottom: 10px; text-align: justify;'>".$publications[$i][3]."</div>";
							echo "</td>";
							echo "</tr>";
						}
					?>
        </table>
        <?php 
				} ?>
      
        <br>
      </td>
      <td style="width:329px; background-color:#F2F2F2; padding:30px; padding-top:40px;">  

      <?php if(sizeof($education) > 0)
        { ?>
         <h3>Education 
         <?php if(sizeof($qualifications) > 0)
					{ ?>
					& Qualifications</h3>
       <?php } else { echo "</h3>"; }
				}
			 if(sizeof($education) > 0)
        { ?>
        <table>
        	<?php 
						for($i = 0; $i < sizeof($education); $i++)	
						{
							echo "<tr><td><ul><li>";
							if($education[$i][2] != "")
								echo "<strong>".$education[$i][2]."<br></strong>";
							if($education[$i][0] != "")
								echo "<strong>".$education[$i][0]."</strong>";
							if($education[$i][4] != "")
								echo "<br><span style='color: #aaaaaa;'>".$education[$i][4]."</span>";
							if($education[$i][5] != "")
								echo "<span style='color: #aaaaaa;'> - ".$education[$i][5]."</span>";
							echo "</li></ul></td></tr>";
							echo "<tr><td>";
							
							if($education[$i][1] != "")
								echo "".$education[$i][1];
							if($education[$i][3] != "")
								echo ", Grade ".$education[$i][3];
							if($education[$i][6] != "")
								echo "<div style='padding-top: 10px; padding-bottom: 10px; text-align: justify;'>".$education[$i][6]."</div>";
							echo "</td>";
							echo "</tr>";
						}
					?>
        </table>
        <?php 
				} ?>
        
        <?php if(sizeof($education) <= 0 && sizeof($qualifications) > 0)
        { ?>
         <h3>Qualifications</h3> 
         <?php
				}?>
        
        <?php if(sizeof($qualifications) > 0)
        { ?>
        <table>
        	<?php 
						for($i = 0; $i < sizeof($qualifications); $i++)	
						{
							echo "<tr>";
							echo "<td><ul><li>";
							if($qualifications[$i][0] != "")
								echo "<strong>".$qualifications[$i][0]."</strong>";
							if($qualifications[$i][2] != "")
								echo " at ".$qualifications[$i][2];
							if($qualifications[$i][1] != "")
								echo " / <span style='color: #aaaaaa;'>".$qualifications[$i][1]."</span>";
							echo "</li></ul></td></tr><tr><td>";
							if($qualifications[$i][3] != "")
								echo "<div style='padding-bottom: 10px; text-align: justify;'>".$qualifications[$i][3]."</div>";
							echo "</td>";
							echo "</tr>";
						}
					?>
        </table>
        <?php 
				} ?>
        
        <?php if(sizeof($skills) > 0)
        { ?>
        <h3>Working Skills 
        <?php if(sizeof($languages) > 0)
        { ?> & Languages
        </h3>
        <?php
				} else {echo "</h3>";}
				?>
        <div style="display: inline-block; width:160px;">
        	<?php 
						$howmany = sizeof($skills);
						$howmany = $howmany/3;
						echo "<div>";
						$j = 0;
						for($i = 0; $i < sizeof($skills); $i++)	
						{
							if($j == $howmany)
							{
								echo "</div><div>";
								$j = 0;
							}
							echo "<strong>".$skills[$i][0]."</strong>: ".$skills[$i][1]."<br>";
							$j++;
						}
						echo "</div>";
					?>
          </div>
        <?php 
				} ?>
        
        <?php if(sizeof($languages) > 0 && sizeof($skills) <= 0)
        { ?>
         <h3>Languages</h3>
        	<?php 
				}?>
        
        <div style="display: inline-block; width:160px;">
        <?php
						for($i = 0; $i < sizeof($languages); $i++)	
						{
							echo "<div>";
							if($languages[$i][0] != "")
								echo "<strong>".$languages[$i][0]."</strong>";
							if($languages[$i][1] != "")
								echo " - ".$languages[$i][1]." level";
							if($languages[$i][2] != "")
								echo "<br>".$languages[$i][2];
							if($languages[$i][3] != "")
								echo " - ".$languages[$i][3];
							echo "</div>";
						}
					?>
         </div>
        
        
      	<?php if($cv['hobbies'] != "")
        { ?>
        <h3>Hobbies & Activities</h3>
        <div style="text-align: justify;">
        	<?php echo $cv['hobbies']; ?>
        </div>
        <?php 
				} ?>

      </td>
    </tr>
  </table>
  </body>
<?php
	//unset($_SESSION['cv']);
	//session_destroy();
?>