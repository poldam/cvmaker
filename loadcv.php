<?php
header('Content-Type: application/json');

function cleanInput($data) {
	$data = str_replace(">","›",$data);
	$data = str_replace("<","‹",$data);
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

function cleanCV($data) {
	
	$data['fullname'] = cleanInput($data['fullname']);
	$data['emailaddress'] = cleanInput($data['emailaddress']);
	$data['websites'] = cleanInput($data['websites']);
	$data['phonenumbers'] = cleanInput($data['phonenumbers']);
	$data['adline1'] = cleanInput($data['adline1']);
	$data['adline2'] = cleanInput($data['adline2']);
	$data['adline3'] = cleanInput($data['adline3']);
	$data['jobtitle'] = cleanInput($data['jobtitle']);
	$data['birthdate'] = cleanInput($data['birthdate']);
	$data['nationality'] = cleanInput($data['nationality']);
	$data['contactdescription'] = cleanInput($data['contactdescription']);
	$data['hobbies'] = cleanInput($data['hobbies']);
	
	if(isset($data['education']))
		for($i = 0; $i < sizeof($data['education']); $i++)
			$data['education'][$i] = cleanInput($data['education'][$i]); 
			
	if(isset($data['workex']))
		for($i = 0; $i < sizeof($data['workex']); $i++)
			$data['workex'][$i] = cleanInput($data['workex'][$i]); 		
			
	if(isset($data['publications']))
		for($i = 0; $i < sizeof($data['publications']); $i++)
			$data['publications'][$i] = cleanInput($data['publications'][$i]);
	
	if(isset($data['awards']))
		for($i = 0; $i < sizeof($data['awards']); $i++)
			$data['awards'][$i] = cleanInput($data['awards'][$i]); 		
	
	if(isset($data['certifications']))
		for($i = 0; $i < sizeof($data['certifications']); $i++)
			$data['certifications'][$i] = cleanInput($data['certifications'][$i]);		
	
	if(isset($data['qualifications']))
		for($i = 0; $i < sizeof($data['qualifications']); $i++)
			$data['qualifications'][$i] = cleanInput($data['qualifications'][$i]);
	
	if(isset($data['skills']))
		for($i = 0; $i < sizeof($data['skills']); $i++)
			$data['skills'][$i] = cleanInput($data['skills'][$i]); 		

	if(isset($data['languages']))
		for($i = 0; $i < sizeof($data['languages']); $i++)
			$data['languages'][$i] = cleanInput($data['languages'][$i]); 
			
  return $data;
}

if(isset($_POST['token']) && $_POST['token'] == "u7y3cohysyiqDT3y9t1hVp32szhIsXlXdW7HTFh1")
{
	$cv = $_POST['cv'];
	$file = "cvs/".$cv;
	//echo "Your CV is saved in JSON format for future use!\n";
	//echo "SAVE: ".json_encode($cvjson);
	$json = file_get_contents($file);
	echo $json;
}
else
{
	echo "Indirect access of the form is not allowed!";
}

?>